/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  10-Aug-2015
 * Description: This Class does below processing
 *              1. Search for member.
 *              2. Change PCP redirect
 *              3. Claims and Authorization fetxh.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            10-Aug-2015              Initial version.
 **************************************************************************************/
public with sharing class Encryption_Check{
    //Global Variables.
    public String strMemberId{get;set;}    
    
    public String mmbrId;
    public List<Contact> con{get;set;}    
            
    public Boolean boolShowClaims{get;set;}
    public Boolean boolShowUpdate{get;set;}
    public Boolean boolShowInfo{get;set;}    
    
    public String strHttpURL;
    public Boolean boolShowCriteriaNullMessage {get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    public List<MemberJSON> MemberValuesUpdated{get;set;}
    public List<MemberJSON> MemberValues{get;set;}
    Public MemberJSON memberInfo {get; set;} 
    //List<Contact> listContacts = new List<Contact>();
   
    public Encryption_Check(){    
        /*mmbrId=ApexPages.CurrentPage().getParameters().get('id');
        if(mmbrId<> NULL){
           con = [select Id, Member_Id__c, firstname, lastname, Birthdate from Contact where id= :mmbrId];
       } */
       boolShowClaims = TRUE;       
       boolShowUpdate = FALSE; 
       boolShowInfo = FALSE;
    }
    /******************** Pagination ***********************/
    String strResBody;
    public void searchButtonClicked() {
       
        con = new List<Contact>();
        system.debug('strMemberId-->'+strMemberId);
       
       if(!String.isBlank(strMemberId)){
        con = [select Id, Member_Id__c, Shield_Encrypted_URL__c, Shield_Encrypted_Phone__c, Shield_Encrypted_Email__c, Encrypted_First_Name__c from Contact where Member_Id__c= :strMemberId];
        
            boolShowClaims = TRUE;                
        }
        else{
            boolShowCriteriaNullMessage = TRUE;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please Enter Atleast One Search Criteria');
            ApexPages.addMessage(myMsg);
        }                            
                boolShowUpdate = FALSE;
                boolShowInfo = TRUE;
                system.debug('MemberValuesUpdated-------------->'+MemberValuesUpdated);
            
            
         
         
    }
    public PageReference LinkClicked(){
        PageReference objRefToMemDtl;
        
        objRefToMemDtl = Page.Member_Detail_Page_Cloned;
        system.debug('### objRefToMemDtl: '+objRefToMemDtl);
        system.debug('### MemberValuesUpdated[0].strMemberId: '+MemberValuesUpdated[0].memberId);
        system.debug('### JSON Response: '+strResBody);
   objRefToMemDtl.getParameters().put('strSearchedMemsJSON',strResBody);
       
        objRefToMemDtl.getParameters().put('strMemberId',MemberValuesUpdated[0].memberId);
        //objRefToMemDtl.getParameters().put('strSearchedMemsJSON',strResBody);
        objRefToMemDtl.setRedirect(true);
        return objRefToMemDtl;
    }
    
    public class MemberJSON{
        public String PlanEffectiveDate{get;set;}
        public String HOMPH2{get;set;}
        public String MemberID{get;set;}
        public String WRKEXT{get;set;}
        public String MemberExpirationDate{get;set;}
        public String SubscriberID{get;set;}
        public String WRKPH1{get;set;}
        public String GroupName{get;set;}
        public String BirthDate{get;set;}
        public String CellPhone{get;set;}
        public String LOB{get;set;}
        public String CompanyNumber{get;set;}
        public String HOMPH1{get;set;}
        public String EmailAddress{get;set;}
        public String City{get;set;}
        public String GroupNumber{get;set;}
        public String Gender{get;set;}
        public String WRKPH2{get;set;}
        public String AddressLine2{get;set;}
        public String ZipCode{get;set;}
        public String OtherPhone{get;set;}
        public String HOMPH3{get;set;}
        public String AddressLine1 {get;set;}
        public String BenefitPackage{get;set;}
        public String WRKPH3{get;set;}
        public String State{get;set;}
        public String PlanExpirationDate{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        
    }
    
     public class Response{
        public String access_token{get;set;}   
        public String scope{get;set;}
        public String token_type{get;set;}
        public String expires_in{get;set;}
    }
}