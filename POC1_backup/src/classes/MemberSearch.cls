/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  10-Aug-2015
 * Description: This Class does below processing
 *              1. Search for member.
 *              2. Member detail page redirect
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            10-Aug-2015              Initial version.
 **************************************************************************************/
public with sharing class MemberSearch{
    //Global Variables.
    public String strMemberId{get;set;}
    public String strResBody;
    
    public String mmbrId;
    public Contact con{get;set;}    
            
    public Boolean boolShowClaims{get;set;}
    public Boolean boolShowUpdate{get;set;}
    public Boolean boolShowInfo{get;set;}    
    
    public String strHttpURL;
    public Boolean boolShowCriteriaNullMessage {get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    public List<MemberJSON> MemberValuesUpdated{get;set;}
    public List<MemberJSON> MemberValues{get;set;}
    Public MemberJSON memberInfo {get; set;} 
    
    public Integer recordsReturned{get;set;}
    
    /*public ApexPages.StandardSetController setConSearchResults{
        get{
            if(listContacts.size() == 0){
                listContacts = (String.isNotBlank(strMemberId))? [SELECT Id, Member_Id__c, firstname, lastname, Subscriber_ID__c, SSN__c FROM Contact WHERE Member_Id__c= :strMemberId] : new List<Contact>();
                setConSearchResults = new ApexPages.StandardSetController(listContacts);
                setConSearchResults.setPageSize(PAGE_SIZE);
                setConSearchResults.setPageNumber(1);
                /*
                String strTemp;
                for(sObject loopCont : setConSearchResults.getRecords()){
                    strTemp = loopCont.get('FirstName')+'';
                    system.debug('### loopCont[FirstName]: '+strTemp);
                    strTemp = loopCont.get('LastName')+'';
                    system.debug('### loopCont[LastName]: '+strTemp);
                }
                */
        /*  }
            return setConSearchResults;
        }
        set;
    }
    public List<Contact> MemberValuesUpdated{
        get{
            return (List<Contact>)setConSearchResults.getRecords();
        }
        set;
    }  
    
    
    public final Integer PAGE_SIZE = 2;*/
    public MemberSearch(){    
        /*mmbrId=ApexPages.CurrentPage().getParameters().get('id');
        if(mmbrId<> NULL){
           con = [select Id, Member_Id__c, firstname, lastname, Birthdate from Contact where id= :mmbrId];
       } */
       boolShowClaims = TRUE;       
       boolShowUpdate = FALSE; 
       boolShowInfo = FALSE;
    }
    /******************** Pagination End***********************/
    
    /******************** Search Member ***********************/
    public PageReference searchButtonClicked() {
        MemberValuesUpdated = new List<MemberJSON>();
       // MemberValues = new List<MemberJSON>();
       
        //MemberValuesUpdated = [select Id, Member_Id__c, firstname, lastname, Subscriber_ID__c, SSN__c from Contact where Member_Id__c= :strMemberId];
        //Pagination.MemberSearchUpdatedStatic = MemberValuesUpdated;
        //Check for the params passing from the page. Make sure at least one param is NOT NULL
        if(!String.isBlank(strMemberId)){
        //||!String.isBlank(strFirstName)||!String.isBlank(strZIPCode)||!String.isBlank(strLastName)||!String.isBlank(strCity)||!String.isBlank(strSSN)||!String.isBlank(strState)||!String.isBlank(strCompany)||!String.isBlank(strAll)||!String.isBlank(strBenefits)||!String.isBlank(strPhone)||strDOB!=null)                    
            boolShowClaims = TRUE;    
            strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/members/';
            strHttpURL += (strMemberId != null && strMemberId != '') ? +strMemberId : '';
            //strHttpURL += '/info';
            
            system.debug('The Endpoint URL is-->'+strHttpURL);
        }
        else{
            boolShowCriteriaNullMessage = TRUE;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please Enter Atleast One Search Criteria');
            ApexPages.addMessage(myMsg);
            return null;
        }         
           
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if(!String.isBlank(strHttpURL)){
            req.setEndpoint(strHttpURL);
            req.setMethod('GET');
            req.setTimeOut(120000);
            
            /* for oauth--------------please do not delete
            req.setHeader('grant_type', 'client_credentials');
            req.setHeader('scope', 'all');
            req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
            req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
            req.setEndpoint(strHttpURL);
            req.setMethod('GET');
            */
        
            try {
                system.debug('THE REQ-->'+req.getBody());
                res = http.send(req);
                System.debug('-----------------'+res);
                System.debug('---------------+STATUS:'+res.getBody());
                system.debug('THE RES-->'+res.getBody());
                strResBody = res.getBody();
                //String strUnwanted = ',"type":"Contact"';                                      
                
                /* for oauth access token and response call--------------please do not delete
                if(!String.isBlank(strResBody))
                {
                    system.debug('Body After Removal-->'+strResBody);
                
                    Response strNewResponse = (Response)JSON.deSerialize(strResBody,Response.class);
                    
                    
                    system.debug('strNewResponse.access_token-->'+strNewResponse.access_token);
                    
                    strHttpURL = 'http://healthfirst-eai-pcp-update.cloudhub.io/pcp?client_id=9b95c64f71e34399b874d63af2ff031f&client_secret=4a5a56f64b474adcA86CBB58D0615406';
                    
                    if(!String.isBlank(strHttpURL))
                    {
                        req = new HttpRequest();
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('Authorization', 'Bearer '+strNewResponse.access_token);
                        req.setEndpoint(strHttpURL);
                        req.setMethod('PUT');
                        req.setTimeOut(120000);
                        req.setBody('{"memberNo": "5000759W5","companyNo": "01","pcpNo": "106566-A26","reasonCode": "M816","actionCode": "E14P","pcpSeenToday": "No"}');
                        res = http.send(req);
                        System.debug('-----------------'+res);
                        System.debug('---------------+STATUS:'+res.getBody());
                        
                        boolShowUpdate = TRUE;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Provider Updated Successfully!');
                        ApexPages.addMessage(myMsg);
                        strUpdateProvider = null;
                        return null;
                    }    
                    return null;
                }
                
                */
                
                if(!String.isBlank(strResBody) && strResBody != null && !strResBody.contains('NullPayload')){
                     system.debug('Body After Removal-->'+strResBody);
                
                
                    system.debug('***'+((list<MemberJSON>)JSON.deSerialize(strResBody,List<MemberJSON>.class)).size() );
                    
                   
                   for(MemberJSON objMember : (List<MemberJSON>)JSON.deSerialize(strResBody,List<MemberJSON>.class) )
                   {           
                         MemberValuesUpdated.add(objMember);
                   }
                   
                   recordsReturned = memberValuesUpdated.size();
                    
                    
                    boolShowUpdate = FALSE;
                    boolShowInfo = TRUE;
                    system.debug('MemberValuesUpdated-------------->'+MemberValuesUpdated);
                    return null;
                }
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Oops! An Error Occurred. Please try again later!');
                ApexPages.addMessage(myMsg);
                return null;
            }
            catch(System.CalloutException e) {
                boolShowResultNullMessage = TRUE;
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Oops! An Error Occurred. Please try again later!');
                strMemberId = null;
                ApexPages.addMessage(myMsg);   
                system.debug('Oops! An Error Occurred! in catch');
                System.debug('Callout error: '+ e);
                System.debug(res.toString());
                return null;
            }
         }
         else{
             system.debug('Oops! An Error Occurred!');
             return null;
         }
         //system.debug('MemberValuesUpdated-------------->'+MemberValuesUpdated);
         //return null;
    }
    /******************** Member Search End ***********************/
    
    /******************** Redirect to Member Detail Page ***********************/
    public PageReference LinkClicked(){
        PageReference objRefToMemDtl;
        
        objRefToMemDtl = Page.Member_Detail_Page;
        system.debug('### objRefToMemDtl: '+objRefToMemDtl);
        system.debug('### MemberValuesUpdated[0].strMemberId: '+MemberValuesUpdated[0].memberId);
        system.debug('### JSON Response: '+strResBody);
   objRefToMemDtl.getParameters().put('strSearchedMemsJSON',strResBody);
       
        objRefToMemDtl.getParameters().put('strMemberId',MemberValuesUpdated[0].memberId);
        //objRefToMemDtl.getParameters().put('strSearchedMemsJSON',strResBody);
        objRefToMemDtl.setRedirect(true);
        return objRefToMemDtl;
    }
    /******************** Redirect End ***********************/
    
    /******************** Wrapper Class for Member JSON ***********************/
    public class MemberJSON{
        public String PlanEffectiveDate{get;set;}
        public String HOMPH2{get;set;}
        public String MemberID{get;set;}
        public String WRKEXT{get;set;}
        public String MemberExpirationDate{get;set;}
        public String SubscriberID{get;set;}
        public String WRKPH1{get;set;}
        public String GroupName{get;set;}
        public String BirthDate{get;set;}
        public String CellPhone{get;set;}
        public String LOB{get;set;}
        public String CompanyNumber{get;set;}
        public String HOMPH1{get;set;}
        public String EmailAddress{get;set;}
        public String City{get;set;}
        public String GroupNumber{get;set;}
        public String Gender{get;set;}
        public String WRKPH2{get;set;}
        public String AddressLine2{get;set;}
        public String ZipCode{get;set;}
        public String OtherPhone{get;set;}
        public String HOMPH3{get;set;}
        public String AddressLine1 {get;set;}
        public String BenefitPackage{get;set;}
        public String WRKPH3{get;set;}
        public String State{get;set;}
        public String PlanExpirationDate{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        
    }
    
    /******************** Wrapper Class for access token ***********************/
     public class Response{
        public String access_token{get;set;}   
        public String scope{get;set;}
        public String token_type{get;set;}
        public String expires_in{get;set;}
    }
}