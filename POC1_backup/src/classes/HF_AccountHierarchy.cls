/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  10-Aug-2015
 * Description: 
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * 
 **************************************************************************************/
public with sharing class HF_AccountHierarchy{

    public Account objAcc{get; set;}
    public static integer intCount;
    public List<Id> lstOfAccHierarchyId{get; set;}
    public List<Account> lstOfAccHierarchy{get; set;}
    public List<Id> lstReturnAcc;
    public List<nodeWrapper> lstNodeWrap{get; set;}
    
    public HF_AccountHierarchy(ApexPages.StandardController controller){
        
        lstReturnAcc = new List<Id>();
        lstNodeWrap = new List<nodeWrapper>();
        intCount = 1;
        lstOfAccHierarchyId = new List<Id>();
        objAcc = new Account();
        objAcc = [SELECT Id, Name, ParentId FROM Account WHERE Id = :ApexPages.currentPage().getParameters().get('id') limit 1];
        if(objAcc != null){
            //Query for Parent records
            lstOfAccHierarchyId.add(objAcc.Id);
            if(objAcc.ParentId != null){
                lstOfAccHierarchyId.add(objAcc.ParentId);
                returnParentofParentAccount(objAcc.ParentId);
            }
            if(!lstOfAccHierarchyId.isEmpty()){
                lstOfAccHierarchy = [Select id, Name, ParentId from Account where Id IN :lstOfAccHierarchyId];
            }
            Integer intPos = 0;
            for(Account objAcc : lstOfAccHierarchy){
                lstNodeWrap.add(new nodeWrapper(intPos,objAcc));
                intPos++;
            }
        }
    }
        
    public void returnParentofParentAccount(Id strAccId){
        System.debug('### Called');
        System.debug('### List before processing : '+lstOfAccHierarchyId);
        intCount++;
        Account objParentAcc = new Account();
        objParentAcc = [SELECT Id, Name, ParentId FROM Account WHERE Id = :strAccId limit 1];
        if(objParentAcc != null && objParentAcc.ParentId != null && intCount<=10){
            lstOfAccHierarchyId.add(objParentAcc.ParentId);
            returnParentofParentAccount(objParentAcc.ParentId);
            System.debug('### List after processing : '+lstOfAccHierarchyId);
        }
        
    }
    
    public class nodeWrapper{
        public Integer intPos{get; set;}
        public Account objAcc{get; set;}
        
        public nodeWrapper(Integer intPos, Account objAcc){
            this.intPos = intPos;
            this.objAcc = objAcc;
        }
        
    }
    
}