/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  19-Aug-2015
 * Description: This Class does below processing
 *              1. Search for member.
 *              2. Upsert member details
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            19-Aug-2015              Initial version.
 **************************************************************************************/
public with sharing class HF_MemberList{
    public List<Contact> MemberValuesUpdated{get;set;} 
    public List<Contact> MemberValuesFetched{get;set;} 
    public List<Contact> MemberValues{get;set;}
    public List<Contact> NewMemberValues{get;set;}
    public String strHttpURL; 
    public Boolean boolShowTable{get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    String strResBody;
    Map<String,Contact> mapMembersValues = new Map<String,Contact>();
    
    public HF_MemberList(){
        boolShowTable = TRUE;
        MemberValues= new List<Contact>();
        MemberValues = [select Id, firstname, lastname, Email,Subscriber_ID__c, SSN__c from Contact];
        for(Contact ObjMember : MemberValues)
        {
            mapMembersValues.put(ObjMember.FirstName+ObjMember.LastName+ObjMember.Email,ObjMember );
        }
    }
    
    public PageReference searchButtonClicked(){
        MemberValuesFetched = new List<Contact>();
        MemberValuesUpdated = new List<Contact>();
        NewMemberValues = new List<Contact>();
        strHttpURL = 'http://choicetest.cloudhub.io/genericService?reqdata=memberall';
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if(!String.isBlank(strHttpURL)){
            req.setEndpoint(strHttpURL);
            req.setMethod('GET');
            req.setTimeOut(120000);
        
            try {
                system.debug('THE REQ-->'+req.getBody());
                res = http.send(req);
                system.debug('THE RES-->'+res.getBody());
                strResBody = res.getBody();
                String strUnwanted = ',"type":"Contact"';                                      
               
                if(!String.isBlank(strResBody)){
                     strResBody = strResBody.remove(strUnwanted);
                     system.debug('Body After Removal-->'+strResBody);
                }
                
                MemberValuesFetched = (List<Contact>)JSON.deSerialize(strResBody,List<Contact>.class);
                
                system.debug('MemberValuesFetched-------------->'+MemberValuesFetched);
                system.debug('Pre Check-------------->'+MemberValues);
                
                 
                for(Contact objMemberFetched : MemberValuesFetched)
                {    
                    
                    objMemberFetched.RecordTypeId='012550000008ZEl';                     
                    if(mapMembersValues != null && mapMembersValues.containsKey(objMemberFetched.FirstName+objMemberFetched.LastName+objMemberFetched.Email))
                    {
                        mapMembersValues.get(objMemberFetched.FirstName+objMemberFetched.LastName+objMemberFetched.Email).Subscriber_ID__c = objMemberFetched.Subscriber_ID__c;
                        mapMembersValues.get(objMemberFetched.FirstName+objMemberFetched.LastName+objMemberFetched.Email).SSN__c = objMemberFetched.SSN__c;
                        MemberValuesUpdated.add(mapMembersValues.get(objMemberFetched.FirstName+objMemberFetched.LastName+objMemberFetched.Email)); 
                        system.debug('Record Updated');
                        system.debug('objMemberFetched#######-------------->'+objMemberFetched);
                        system.debug('MemberValues after Updated-------------->'+MemberValuesUpdated);    
                    }
                    else
                    {
                        objMemberFetched.Id=null; 
                        MemberValuesUpdated.add(objMemberFetched);
                    }                   
                }                
                system.debug('MemberValues after Updated############-------------->'+MemberValuesUpdated);
                upsert MemberValuesUpdated;
                
                MemberValues = [select Id, firstname, lastname, Email,Subscriber_ID__c, SSN__c from Contact];
                for(Contact ObjMember : MemberValues)
                {
                    mapMembersValues.put(ObjMember.FirstName+ObjMember.LastName+ObjMember.Email,ObjMember );
                }
            }
            catch(System.CalloutException e) {
                boolShowResultNullMessage = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Oops! An Error Occurred.');
                ApexPages.addMessage(myMsg);   
                system.debug('Oops! An Error Occurred!');
                System.debug('Callout error: '+ e);
                System.debug(res.toString());
            }
         }
         else{
             system.debug('Oops! An Error Occurred!');
         }
         return null;
    }


}