/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  10-Aug-2015
 * Description: This Class does below processing
 *              1. Search for member.
 *              2. member detail page redirect
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            02-Sep-2015              Initial version.
 **************************************************************************************/
public with sharing class MemberDetail_Cloned{
    //Global Variables.

    public String mmbrId;
    public MemberJSON con{get;set;}    
    
    public String strMemberId{get;set;}
    //public String strProvierNumber{get;set;}
    //public String strCheckNumber{get;set;}
    //public String strClaimsNumber{get;set;}
    //public String strExternalClaimsNumber{get;set;}
    public Boolean boolShowClaims{get;set;}
    public Boolean boolAuthInfo{get;set;}
    public Boolean boolClaimsInfo{get;set;}
    public Boolean boolShowUpdate{get;set;}
    public Boolean boolShowClearClaims{get;set;}
    public Boolean boolShowClearAuth{get;set;}
    public String strSelectDB{get;set;}
    
    public String strHttpURL;
    public Boolean boolShowCriteriaNullMessage {get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    public List<ClaimsJSON> ClaimValues{get;set;}
    public List<ClaimsJSON> ClaimValuesUpdated{get;set;}
    public List<AuthJSON> AuthValues{get;set;}
    public List<AuthJSON> AuthValuesUpdated{get;set;} 
    public MemberJSON listMembersSearched{get; set;}
    public Contact dummyContact1 {get; set;}
    public Contact dummyContact2 {get; set;}
    public Contact dummyContact3 {get; set;}
    public Contact dummyContact4 {get; set;}
    public String strSearchedMemsJSON;
    
   
    public MemberDetail_Cloned()
    {   
        mmbrId=ApexPages.CurrentPage().getParameters().get('id');
        strMemberID = mmbrId;
        dummyContact1 = new Contact();
        dummyContact2 = new Contact();
        dummyContact3 = new Contact();
        dummyContact4 = new Contact();
        
        //checking for member JSON
        if(ApexPages.CurrentPage().getParameters().containsKey('strSearchedMemsJSON') && String.isNotBlank(ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON'))){
            strSearchedMemsJSON = ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON');
            list<MemberJSON> con = new List<MemberJSON>();
            con = (List<MemberJSON>)JSON.deSerialize(strSearchedMemsJSON,List<MemberJSON>.class);
            
            system.debug('### listMembersSearched.size: '+con);
            listMembersSearched = new MemberJSON();
            listMembersSearched = con[0];
            
            if(listMembersSearched.BirthDate!=null && listMembersSearched.BirthDate!=''){
                String strBirthDate = listMembersSearched.BirthDate;
                listMembersSearched.BirthDate = strBirthDate.substring(3,5)+'-'+strBirthDate.substring(5,7)+'-'+strBirthDate.substring(0,1)+(strBirthDate.substring(0,1) == '2' ? '0' : '9')+strBirthDate.substring(1,3);
            }
            
            if(listMembersSearched.PlanExpirationDate!=null && listMembersSearched.PlanExpirationDate!=''){
                String strPlanExpirationDate = listMembersSearched.PlanExpirationDate;
                listMembersSearched.PlanExpirationDate = strPlanExpirationDate.substring(3,5)+'-'+strPlanExpirationDate.substring(5,7)+'-'+strPlanExpirationDate.substring(0,1)+(strPlanExpirationDate.substring(0,1) == '2' ? '0' : '9')+strPlanExpirationDate.substring(1,3);
            }
            
            if(listMembersSearched.MemberExpirationDate!=null && listMembersSearched.MemberExpirationDate!=''){
                String strMemberExpirationDate = listMembersSearched.MemberExpirationDate;
                listMembersSearched.MemberExpirationDate = strMemberExpirationDate.substring(3,5)+'-'+strMemberExpirationDate.substring(5,7)+'-'+strMemberExpirationDate.substring(0,1)+(strMemberExpirationDate.substring(0,1) == '2' ? '0' : '9')+strMemberExpirationDate.substring(1,3);
            }
            
            if(listMembersSearched.PlanEffectiveDate!=null && listMembersSearched.PlanEffectiveDate!=''){
                String strPlanEffectiveDate = listMembersSearched.PlanEffectiveDate;
                listMembersSearched.PlanEffectiveDate = strPlanEffectiveDate.substring(3,5)+'-'+strPlanEffectiveDate.substring(5,7)+'-'+strPlanEffectiveDate.substring(0,1)+(strPlanEffectiveDate.substring(0,1) == '2' ? '0' : '9')+strPlanEffectiveDate.substring(1,3);
            }
        }
        
        boolShowClaims = TRUE;  
        boolClaimsInfo = FALSE;
        boolAuthInfo = FALSE;     
    }
    
    //redirecting to Update PCP page, passing member JSON to maintain member reference    
    public PageReference ChangePCP() {
        PageReference objRefToChangePCP;
        objRefToChangePCP = Page.HF_ChangePCP_Cloned;
        objRefToChangePCP.getParameters().put('id',mmbrId);
        objRefToChangePCP.getParameters().put('retURL','/apex/Member_Detail_Page?id='+mmbrId);
        objRefToChangePCP.getParameters().put('strSearchedMemsJSON',strSearchedMemsJSON);
        objRefToChangePCP.setRedirect(true);
        return objRefToChangePCP;
    }
    
    //for Calims response
    public PageReference CustomFunction() {
        Long strDateClaims = DateTime.now().getTime();
        system.debug('Start Time on Claims------>'+strDateClaims);
        boolShowUpdate = false;
        ClaimValuesUpdated = new List<ClaimsJSON>();
        
        //checking for member details available
        if(listMembersSearched!=null)
        {
            //check for required input parameters available
            if(!String.isBlank(listMembersSearched.memberId) && dummyContact1.BirthDate != null && dummyContact2.BirthDate != null)
            {    
                boolShowClaims = TRUE; 
                strHttpURL = 'https://hf-eai-oauthprovider-dev.cloudhub.io/access-token';
                /*
                //creating URL for making http callout to external db
                 
                //claims url
                strHttpURL = 'http://hf-poc-demo-v1.cloudhub.io/api/claims?';
                //end date or TO date
                strHttpURL += (dummyContact2.BirthDate != null) ? 'endDate='+(String.valueOf(dummyContact2.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact2.BirthDate.Year())).substring(2,4)+(dummyContact2.BirthDate.month()>9?'':'0')+dummyContact2.BirthDate.month()+(dummyContact2.BirthDate.day()>9?'':'0')+dummyContact2.BirthDate.day() : '';
                //  strHttpURL +='&memberId=';
                strHttpURL += (listMembersSearched.MemberId != null && listMembersSearched.MemberId != '') ? '&memberId='+listMembersSearched.MemberId : '';
                //start date or From date
                strHttpURL += (dummyContact1.BirthDate != null) ? '&startDate='+(String.valueOf(dummyContact1.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact1.BirthDate.Year())).substring(2,4)+(dummyContact1.BirthDate.month()>9?'':'0')+dummyContact1.BirthDate.month()+(dummyContact1.BirthDate.day()>9?'':'0')+dummyContact1.BirthDate.day() : '';
               
                system.debug('The Endpoint URL is-->'+strHttpURL);*/
            }
            else
            {
                //inputs missing, display error message
                boolShowCriteriaNullMessage = TRUE;
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please check Member Id and confirm From Date and To Date to fetch Claims!');
                ApexPages.addMessage(myMsg);
                return null;
            }         
        }   
        else{
            //member details not available
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Member Details not available!');
            ApexPages.addMessage(myMsg);
            boolShowUpdate = TRUE;
            boolShowClearClaims = TRUE;
            return null;
        }
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if(!String.isBlank(strHttpURL))
        {
            req.setHeader('grant_type', 'client_credentials');
            req.setHeader('scope', 'WRITE');
            req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
            req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
            req.setEndpoint(strHttpURL);
            req.setTimeOut(120000);
            req.setMethod('GET');
        
            try 
            {
                //making callout to external db to fetch claims
                res = http.send(req);
                system.debug('THE RES-->'+res.getBody());
                String strResBody = res.getBody();
                
                if(!String.isBlank(strResBody) && strResBody != null && (!strResBody.contains('NullPayload') || !strResBody.contains('Resource not found') || !strResBody.contains('Gateway Time-out')))
                {
                    system.debug('Body After Removal-->'+strResBody);
                
                    Response strNewResponse = (Response)JSON.deSerialize(strResBody,Response.class);
                    
                    system.debug('strNewResponse.access_token-->'+strNewResponse.access_token);
                
                    strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/claims?';
                    //end date or TO date
                    strHttpURL += (dummyContact2.BirthDate != null) ? 'endDate='+(String.valueOf(dummyContact2.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact2.BirthDate.Year())).substring(2,4)+(dummyContact2.BirthDate.month()>9?'':'0')+dummyContact2.BirthDate.month()+(dummyContact2.BirthDate.day()>9?'':'0')+dummyContact2.BirthDate.day() : '';
                    //  strHttpURL +='&memberId=';
                    strHttpURL += (listMembersSearched.MemberId != null && listMembersSearched.MemberId != '') ? '&memberId='+listMembersSearched.MemberId : '';
                    //start date or From date
                    strHttpURL += (dummyContact1.BirthDate != null) ? '&startDate='+(String.valueOf(dummyContact1.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact1.BirthDate.Year())).substring(2,4)+(dummyContact1.BirthDate.month()>9?'':'0')+dummyContact1.BirthDate.month()+(dummyContact1.BirthDate.day()>9?'':'0')+dummyContact1.BirthDate.day() : '';
                    strHttpURL = strHttpURL.replaceAll('(\\s+)', '');
                    system.debug('The Endpoint URL is-->'+strHttpURL);
                    
                    if(!String.isBlank(strHttpURL))
                    {
                        req = new HttpRequest();
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('Authorization', 'Bearer '+strNewResponse.access_token);
                        req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
                        req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
                        req.setEndpoint(strHttpURL);
                        req.setMethod('GET');
                        req.setTimeOut(120000);
                        //req.setBody('{"memberNo": "5000759W5","companyNo": "01","pcpNo": "106566-A26","reasonCode": "M816","actionCode": "E14P","pcpSeenToday": "No"}');
                        res = http.send(req);
                        System.debug('-----------------'+res);
                        System.debug('---------------+STATUS:'+res.getBody());
                        strResBody = res.getBody();
                
                        //checking for response
                        if(!String.isBlank(strResBody) && strResBody != null && !strResBody.contains('NullPayload'))
                        {
                             system.debug('Body After Removal-->'+strResBody);
                        
                        
                            ClaimValues = (List<ClaimsJSON>)JSON.deSerialize(strResBody,List<ClaimsJSON>.class);
                            
                            for(ClaimsJSON objClaim : ClaimValues) 
                            {                         
                                 ClaimValuesUpdated.add(objClaim);
                            }
                            
                            if(ClaimValuesUpdated!= null && ClaimValuesUpdated.isEmpty()){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No records to display.');
                                ApexPages.addMessage(myMsg);
                                boolShowUpdate = TRUE;
                                boolShowClearClaims = TRUE;
                                return null;
                            }
                            
                            system.debug('ClaimValuesUpdated-------------->'+ClaimValuesUpdated);
                            boolClaimsInfo = TRUE;
                            boolShowClearClaims = TRUE;
                            
                            Long strNewDateClaims = DateTime.now().getTime() - strDateClaims;
                            system.debug('End Time on claims------>'+DateTime.now().getTime());
                            system.debug('TimeStamp on Claims call-------------->'+strNewDateClaims);  
                            return null;
                        }
                    }
                }
                //no records retrived in callout
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Data available.');
                ApexPages.addMessage(myMsg);
                boolShowUpdate = TRUE;
                boolShowClearClaims = TRUE;
                return null;
            }
             
            catch(System.CalloutException e) 
            {
                boolShowResultNullMessage = TRUE;
                boolShowUpdate = TRUE;
                boolShowClearClaims = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Oops! An Error Occurred.');
                ApexPages.addMessage(myMsg);
                Long strNewDateClaims = DateTime.now().getTime() - strDateClaims;
                system.debug('End Time-->'+DateTime.now().getTime());
                system.debug('TimeStamp on Claims call fail-------------->'+strNewDateClaims);
                system.debug('Oops! An Error Occurred!');
                System.debug('Callout error: '+ e);
                System.debug(res.toString());
                return null;
            }
            return null;
         }
         else
         {
            system.debug('Oops! An Error Occurred! Callout failed');
            return null;
         }
            
       }
       
       //traversing to member search page
       public PageReference backButtonClicked(){
            PageReference objRefToMemSearch;
            
            objRefToMemSearch = Page.HF_MemberSearch_Cloned;
            objRefToMemSearch.getParameters().put('id',strMemberID);
            objRefToMemSearch.setRedirect(true);
            return objRefToMemSearch;
       }
       
       //method for clearing claims
       public void ClearClaims(){
           boolShowUpdate = False;
           ClaimValuesUpdated = new List<ClaimsJSON>();
           dummyContact1.BirthDate = null;
           dummyContact2.BirthDate = null;
           boolClaimsInfo = FALSE;
           boolShowClearClaims = FALSE;
       }
       
       //for Auth response
       public PageReference CustomFunction1() {
        
        Long strDateAuth = DateTime.now().getTime();
        system.debug('Start Time on Auth------>'+strDateAuth);
        
        AuthValuesUpdated = new List<AuthJSON>();
        system.debug('strSelectDB------------------------>'+strSelectDB);
        //checking for member details available
        if(listMembersSearched!=null)
        {
            //check for required input parameters available
            if(!String.isBlank(listMembersSearched.MemberId ) && dummyContact3.BirthDate != null && dummyContact4.BirthDate != null)
            {   
                boolShowClaims = TRUE; 
                strHttpURL = 'https://hf-eai-oauthprovider-dev.cloudhub.io/access-token';
                /*
                //creating URL for making http callout to external db
                //auth URL
                strHttpURL = 'http://hf-poc-demo-v1.cloudhub.io/api/auths?';
                //checking for database MHS or ODR
                system.debug('--->><<---'+strSelectDB);
                if(strSelectDB=='selectDB_MHS')
                    strHttpURL += 'reqdb=mhs&';
                else if(strSelectDB=='selectDB_ODR')
                    strHttpURL += 'reqdb=odr&';
                //end date or TO date
                strHttpURL += (dummyContact4.BirthDate != null) ? 'endDate='+(String.valueOf(dummyContact4.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact4.BirthDate.Year())).substring(2,4)+(dummyContact4.BirthDate.month()>9?'':'0')+dummyContact4.BirthDate.month()+(dummyContact4.BirthDate.day()>9?'':'0')+dummyContact4.BirthDate.day() : '';
                //  strHttpURL +='&memberId=';
                strHttpURL += (listMembersSearched.MemberId  != null && listMembersSearched.MemberId  != '') ? '&memberId='+listMembersSearched.MemberId : '';
                //start date or From date
                strHttpURL += (dummyContact3.BirthDate != null) ? '&startDate='+(String.valueOf(dummyContact3.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact3.BirthDate.Year())).substring(2,4)+(dummyContact3.BirthDate.month()>9?'':'0')+dummyContact3.BirthDate.month()+(dummyContact3.BirthDate.day()>9?'':'0')+dummyContact3.BirthDate.day() : '';
                strHttpURL = strHttpURL.replaceAll('(\\s+)', ' ');
                system.debug('The Endpoint URL is-->'+strHttpURL);*/
            }
            else
            {
                //inputs missing, display error message
                boolShowUpdate = TRUE;
                boolShowCriteriaNullMessage = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please check Member Id and confirm From Date and To Date to fetch Authorizations!');
                ApexPages.addMessage(myMsg);
                return null;
            } 
        }
        else{
                //member details not available
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Member Details not available!');
                ApexPages.addMessage(myMsg);
                boolShowUpdate = TRUE;
                boolShowClearAuth = TRUE;
                return null;
        }
               
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            if(!String.isBlank(strHttpURL))
            {
                req.setHeader('grant_type', 'client_credentials');
                req.setHeader('scope', 'WRITE');
                req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
                req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
                req.setEndpoint(strHttpURL);
                //req.setTimeOut(120000);
                req.setMethod('GET');
            
                try 
                {
                    //making callout to external db to fetch auth
                    res = http.send(req);
                    system.debug('THE RES-->'+res.getBody());
                    String strResBody = res.getBody();
                    
                    if(!String.isBlank(strResBody) && strResBody != null && (!strResBody.contains('NullPayload') || !strResBody.contains('Resource not found') || !strResBody.contains('Gateway Time-out')))
                    {
                        system.debug('Body After Removal-->'+strResBody);
                    
                        Response strNewResponse = (Response)JSON.deSerialize(strResBody,Response.class);
                        
                        system.debug('strNewResponse.access_token-->'+strNewResponse.access_token);
                    
                        //auth URL
                        strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/auths?';
                        //checking for database MHS or ODR
                        system.debug('--->><<---'+strSelectDB);
                        if(strSelectDB=='selectDB_MHS')
                            strHttpURL += 'reqdb=mhs&';
                        else if(strSelectDB=='selectDB_ODR')
                            strHttpURL += 'reqdb=odr&';
                        //end date or TO date
                        strHttpURL += (dummyContact4.BirthDate != null) ? 'endDate='+(String.valueOf(dummyContact4.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact4.BirthDate.Year())).substring(2,4)+(dummyContact4.BirthDate.month()>9?'':'0')+dummyContact4.BirthDate.month()+(dummyContact4.BirthDate.day()>9?'':'0')+dummyContact4.BirthDate.day() : '';
                        //  strHttpURL +='&memberId=';
                        strHttpURL += (listMembersSearched.MemberId  != null && listMembersSearched.MemberId  != '') ? '&memberId='+listMembersSearched.MemberId : '';
                        //start date or From date
                        strHttpURL += (dummyContact3.BirthDate != null) ? '&startDate='+(String.valueOf(dummyContact3.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact3.BirthDate.Year())).substring(2,4)+(dummyContact3.BirthDate.month()>9?'':'0')+dummyContact3.BirthDate.month()+(dummyContact3.BirthDate.day()>9?'':'0')+dummyContact3.BirthDate.day() : '';
                        strHttpURL = strHttpURL.replaceAll('(\\s+)', '');
                        system.debug('The Endpoint URL is-->'+strHttpURL);
                        
                        if(!String.isBlank(strHttpURL))
                        {
                            req = new HttpRequest();
                            req.setHeader('Content-Type', 'application/json');
                            req.setHeader('Authorization', 'Bearer '+strNewResponse.access_token);
                            req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
                            req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
                            req.setEndpoint(strHttpURL);
                            req.setMethod('GET');
                            //req.setTimeOut(120000);
                            //req.setBody('{"memberNo": "5000759W5","companyNo": "01","pcpNo": "106566-A26","reasonCode": "M816","actionCode": "E14P","pcpSeenToday": "No"}');
                            res = http.send(req);
                            System.debug('-----------------'+res);
                            System.debug('---------------+STATUS:'+res.getBody());
                            strResBody = res.getBody();
                       
                            //checking for response
                            if(!String.isBlank(strResBody) && strResBody != null && !strResBody.contains('NullPayload'))
                            {
                                system.debug('Body After Removal-->'+strResBody);
                            
                                AuthValues = (List<AuthJSON>)JSON.deSerialize(strResBody,List<AuthJSON>.class);
                                
                                for(AuthJSON objAuth : AuthValues) 
                                {         
                                    if(objAuth.EffectiveDate!=null && objAuth.EffectiveDate!=''){
                                        String strEffectiveDate = objAuth.EffectiveDate;
                                        objAuth.EffectiveDate = strEffectiveDate.substring(3,5)+'-'+strEffectiveDate.substring(5,7)+'-'+strEffectiveDate.substring(0,1)+(strEffectiveDate.substring(0,1) == '2' ? '0' : '9')+strEffectiveDate.substring(1,3);
                                    }
                                    
                                    if(objAuth.ExpirationDate!=null && objAuth.ExpirationDate!=''){
                                        String strExpirationDate = objAuth.ExpirationDate;
                                        objAuth.ExpirationDate = strExpirationDate.substring(3,5)+'-'+strExpirationDate.substring(5,7)+'-'+strExpirationDate.substring(0,1)+(strExpirationDate.substring(0,1) == '2' ? '0' : '9')+strExpirationDate.substring(1,3);
                                    }
                                    AuthValuesUpdated.add(objAuth);
                                }
                                
                                if(AuthValuesUpdated!= null && AuthValuesUpdated.isEmpty()){
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No records to display.');
                                    ApexPages.addMessage(myMsg);
                                    boolShowUpdate = TRUE;
                                    boolShowClearAuth = TRUE;
                                    return null;
                                }
                                system.debug(AuthValues);
                                boolAuthInfo = TRUE;
                                boolShowClearAuth = TRUE;
                                
                                Long strNewDateAuth = DateTime.now().getTime() - strDateAuth;
                                system.debug('End Time on Auth----->'+DateTime.now().getTime());
                                system.debug('TimeStamp on Auth-------------->'+strNewDateAuth); 
                                return null;
                            }
                        }
                    }
                    //no records retrived in callout
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Data available.');
                    ApexPages.addMessage(myMsg);
                    boolShowUpdate = TRUE;
                    boolShowClearAuth = TRUE;
                    return null;
                }
                 
                catch(System.CalloutException e) 
                {
                    boolShowResultNullMessage = TRUE;
                    boolShowUpdate = TRUE;
                    boolShowClearAuth = TRUE;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Oops! An Error Occurred.');
                    ApexPages.addMessage(myMsg); 
                    Long strNewDateAuth = DateTime.now().getTime() - strDateAuth;
                    system.debug('End Time Auth Call-->'+DateTime.now().getTime());
                    system.debug('TimeStamp on auth Call fail-------------->'+strNewDateAuth); 
                    system.debug('Oops! An Error Occurred!');
                    System.debug('Callout error: '+ e);
                    System.debug(res.toString());
                    return null;
                }
                return null;
             }
             else
             {
                 system.debug('Oops! An Error Occurred!');
                 return null;
             }
       }
       
       //method for clearing auth
       public void ClearAuth(){
           AuthValuesUpdated = new List<AuthJSON>();
           dummyContact3.BirthDate = null;
           dummyContact4.BirthDate = null;
           boolAuthInfo = FALSE;
           boolShowClearAuth = FALSE;
       }
      
      /******************************
      ### wrapper class for Member
      */ 
      public class MemberJSON{
        public String PlanEffectiveDate{get;set;}
        public String HOMPH2{get;set;}
        public String MemberID{get;set;}
        public String WRKEXT{get;set;}
        public String MemberExpirationDate{get;set;}
        public String SubscriberID{get;set;}
        public String WRKPH1{get;set;}
        public String GroupName{get;set;}
        public String BirthDate{get;set;}
        public String CellPhone{get;set;}
        public String LOB{get;set;}
        public String CompanyNumber{get;set;}
        public String HOMPH1{get;set;}
        public String EmailAddress{get;set;}
        public String City{get;set;}
        public String GroupNumber{get;set;}
        public String Gender{get;set;}
        public String WRKPH2{get;set;}
        public String AddressLine2{get;set;}
        public String ZipCode{get;set;}
        public String OtherPhone{get;set;}
        public String HOMPH3{get;set;}
        public String AddressLine1 {get;set;}
        public String BenefitPackage{get;set;}
        public String WRKPH3{get;set;}
        public String State{get;set;}
        public String PlanExpirationDate{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        
    }
    
    /******************************
    ### wrapper class for Claims
    */
    public class ClaimsJSON{
        public String PayAmount {get;set;}
        public String ClaimAmount {get;set;}
        public String AllowedAmount{get;set;}
        public String CopayAmount {get;set;}
        public String PrepayAmount{get;set;}
        public String DeductibleAmount{get;set;}
        public String COBAmount{get;set;}
        public String CoinsuranceAmount{get;set;}
        public String WithholdAmount{get;set;}

    }
    
    /******************************
    ### wrapper class for auth
    */
    public class AuthJSON{
        public String expDate{get;set;}
        public String effDate{get;set;}
        public String authNo{get;set;}   
        
        public String AuthorizationNumber{get;set;} 
        public String EffectiveDate{get;set;} 
        public String ExpirationDate{get;set;}  
        
        public String FromProviderID{get;set;} 
        public String FromProviderLastName{get;set;} 
        public String FromProviderFirstName{get;set;} 
        
        
        public String ToProviderID{get;set;} 
        public String ToProviderLastName{get;set;} 
        public String ToProviderFirstName {get;set;} 
        
    }
    
    /******************************
    ### wrapper class for response
    */
    public class Response{
        public String access_token{get;set;}   
        public String scope{get;set;}
        public String token_type{get;set;}
        public String expires_in{get;set;}
    }
}