/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  28-Sep-2015
 * Description: This Class does below processing
 *              1. Select Category
 *              2. Assign message based on selection
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            28-Sep-2015              Initial version.
 **************************************************************************************/
public with sharing class Broadcast_Message_HF{
    //Global variables
    public List<User> lstUser{get;set;}
    public List<User> lstAllUser{get;set;}
    public List<User> lstUserAdHoc{get;set;}
    public List<UserRole> listUserRole{get;set;}
    public List<Group>lstPublicGroup{get;set;}
    public List<Group>lstQueues{get;set;}
    public String strSelect{get;set;}
    public String strLOB{get;set;}
    public String strRoles{get;set;}
    public String strR_S{get;set;}
    public String strQueues{get;set;}
    public String strPublicGroup{get;set;}
    public String strIdChoosen{get;set;}
    public String strCheckSelection{get;set;}
    public set<String> setSelectUser = new set<string>();
    public set<String> setGlobalLst = new set<string>();
    public List<User> lstGlobalUser{get;set;}
    public String strMessageId{get;set;}
    public static final String strNone = '-None-';
    
    //Boolean variables
    public Boolean boolAdHoc{get;set;}
    public Boolean boolLOB{get;set;}
    public Boolean boolLOBList{get;set;}
    public Boolean boolRoles{get;set;}
    public Boolean boolR_S{get;set;}
    public Boolean boolQueue{get;set;}
    public Boolean boolPublicGroup{get;set;}
    public Boolean boolSave{get;set;}
    public Boolean boolUsers{get;set;}
    public Boolean boolAllUsers{get;set;}
    
    public Broadcast_Message_HF(){
        boolAdHoc = false;
        boolLOB = false;
        boolRoles = false;
        boolR_S = false;
        boolUsers = false;
        lstUser = new List<User>();
        lstAllUser = new List<User>();
        //listSelectUser = new set<String>();
        //listUser = [SELECT Id, EmployeeNumber, IsActive, firstname, lastname, Username FROM User];
        strMessageId = ApexPages.currentPage().getParameters().get('id');
        
    }
    
    //List for category selection
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(strNone,strNone));
        string val = System.Label.Category_MessageBroadcast_HF;
        List<string> strList = val.split(',');
        for(integer i=0;i<strList.size();i++){
            options.add(new SelectOption(strList[i],strList[i]));
        }
        return options;
    }
    
    //List for LOB selection
    public List<SelectOption> getItemsLOB() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(strNone,strNone));
        Schema.DescribeFieldResult fieldResult =User.Line_of_Business_HF__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    
    //List for Roles selection
    public List<SelectOption> getItemsRoles() {
        listUserRole = new List<UserRole>();
        listUserRole = [SELECT Id, Name FROM UserRole];
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(strNone,strNone));
        for(UserRole objUserRole : listUserRole){
            options.add(new SelectOption(objUserRole.id,objUserRole.name));
        }
        return options;
    }
    
    //List for Roles and Subordinates selection
    public List<SelectOption> getItemsR_S() {
        List<SelectOption> options = new List<SelectOption>();
        listUserRole = new List<UserRole>();
        listUserRole = [SELECT Id, Name FROM UserRole];
        options.add(new SelectOption(strNone,strNone));
        for(UserRole objUserRole : listUserRole){
            options.add(new SelectOption(objUserRole.id,objUserRole.name));
        }
        return options;
    }
    
    //List for Public Group selection
    public List<SelectOption> getItemsPublic_Groups() {
        List<SelectOption> options = new List<SelectOption>();
        lstPublicGroup = new List<Group>();
        lstPublicGroup = [SELECT Id, Name FROM Group where type =:'Regular'];
        options.add(new SelectOption(strNone,strNone));
        for(Group objGroup : lstPublicGroup){
            options.add(new SelectOption(objGroup.id,objGroup.name));
        }
        return options;
    }
    
    //List for Queues selection
    public List<SelectOption> getItems_Queues() {
        List<SelectOption> options = new List<SelectOption>();
        lstQueues = new List<Group>();
        lstQueues = [SELECT Id, Name FROM Group where type=:'Queue'];
        options.add(new SelectOption(strNone,strNone));
        for(Group objQueue : lstQueues){
            options.add(new SelectOption(objQueue.id,objQueue.name));
        }
        return options;
    }
    
    //Check for category selection    
    public void displaySection(){
        if(strSelect == 'Ad Hoc'){
            boolAdHoc = true;
            boolLOB = false;
            boolRoles = false;
            boolR_S = false;
            boolUsers = false;
            boolQueue = false;
            boolPublicGroup = false;
            lstUserAdHoc = [SELECT Id, EmployeeNumber, IsActive, firstname, lastname, Username FROM User WHERE IsActive = true];
        }
        else if(strSelect == 'Line of Business'){
            boolAdHoc = false;
            boolLOB = true;
            boolRoles = false;
            boolR_S = false;
            boolUsers = false;
            boolQueue = false;
            boolPublicGroup = false;
        }
        else if(strSelect == 'Roles'){
            boolAdHoc = false;
            boolLOB = false;
            boolRoles = true;
            boolR_S = false;
            boolUsers = false;
            boolQueue = false;
            boolPublicGroup = false;
        }
        else if(strSelect == 'Roles and Subordinates'){
            boolAdHoc = false;
            boolLOB = false;
            boolRoles = false;
            boolR_S = true;
            boolUsers = false;
            boolQueue = false;
            boolPublicGroup = false;
        }
        else if(strSelect == 'All Users'){
            boolAdHoc = false;
            boolLOB = false;
            boolRoles = false;
            boolR_S = false;
            boolUsers = true;
        }
        else if(strSelect == 'Public Groups'){
            boolAdHoc = false;
            boolLOB = false;
            boolRoles = false;
            boolR_S = false;
            boolUsers = false;
            boolQueue = false;
            boolPublicGroup = true;
        }
        else if(strSelect == 'Queues'){
            boolAdHoc = false;
            boolLOB = false;
            boolRoles = false;
            boolR_S = false;
            boolUsers = false;
            boolQueue = true;
            boolPublicGroup = false;
        }
        else{
            boolAdHoc = false;
            boolLOB = false;
            boolRoles = false;
            boolR_S = false;
            boolUsers = false;
            boolQueue = false;
            boolPublicGroup = false;
        }
    }
    
    public void SaveMethod(){
        //assigning message based on User selection
        if(boolAdHoc){
            try{
                //set<String> setListUser = new set<String>();
                set<String> setUMA = new set<String>();
                
                for(HF_User_Messages_Association__c objAssoc: [select HF_User__c from HF_User_Messages_Association__c where HF_User__c IN :setSelectUser AND HF_Broadcast_Message__c =:strMessageId]){
                    setUMA.add(objAssoc.HF_User__c);
                }
                
                List<HF_User_Messages_Association__c> lstUMAssco = new List<HF_User_Messages_Association__c>();
                
                for(Id objU : setSelectUser){
                    //lstUMAssco.add(new HF_User_Messages_Association__c(HF_User__c = objU.id, HF_Broadcast_Message__c = strMessageId));
                    
                    if(!setUMA.contains(objU)){
                        system.debug('record not present');
                        HF_User_Messages_Association__c objUMA = new HF_User_Messages_Association__c();
                        objUMA.HF_User__c = objU;
                        objUMA.HF_Broadcast_Message__c = strMessageId;
                        lstUMAssco.add(objUMA);
                    }
                    //objUMA.HF_User__c = objU.id;
                    //objUMA.HF_Broadcast_Message__c = strMessageId;
                    //lstUMAssco.add(objUMA);
                }
                system.debug('lstUMAssco----------'+lstUMAssco);
                if(lstUMAssco.size()>0){
                   insert lstUMAssco; 
                }
                setSelectUser = new set<String>();
            }
            catch(System.CalloutException e){
                System.debug('Callout error: '+ e);
            }
        }
        //assigning message based on LOB selection
        else if(boolLOB){
            try{
                system.debug('Save LOB---------'+strLOB);
                lstUser = [SELECT Id, EmployeeNumber, IsActive, firstname, lastname, Username FROM User];// where Line_of_Business_HF__c :strLOB];
                system.debug('lstUser---------'+lstUser);
                /*List<HF_User_Messages_Association__c> lstUMAssco = new List<HF_User_Messages_Association__c>();
                
                for(User objU : lstUser){
                    //lstUMAssco.add(new HF_User_Messages_Association__c(HF_User__c = objU.id, HF_Broadcast_Message__c = strMessageId));
                    HF_User_Messages_Association__c objUMA = new HF_User_Messages_Association__c();
                    objUMA.HF_User__c = objU.id;
                    objUMA.HF_Broadcast_Message__c = strMessageId;
                    lstUMAssco.add(objUMA);
                }
                system.debug('lstUMAssco----------'+lstUMAssco);
                insert lstUMAssco;*/
                
            }
            catch(System.CalloutException e){
                System.debug('Callout error: '+ e);
            }
        }
        //assigning message based on Roles selection
        else if(boolRoles){
            try{
                system.debug('Save Roles---------'+strRoles);
                lstUser = new List<User>();
                lstUser = [SELECT Id, EmployeeNumber, IsActive, firstname, lastname, Username FROM User where UserRole.id =:strRoles];// where Id IN :listSelectUser];
                system.debug('lstUser---------'+lstUser);
                List<HF_User_Messages_Association__c> lstUMAssco = new List<HF_User_Messages_Association__c>();
                
                for(User objU : lstUser){
                    //lstUMAssco.add(new HF_User_Messages_Association__c(HF_User__c = objU.id, HF_Broadcast_Message__c = strMessageId));
                    HF_User_Messages_Association__c objUMA = new HF_User_Messages_Association__c();
                    objUMA.HF_User__c = objU.id;
                    objUMA.HF_Broadcast_Message__c = strMessageId;
                    lstUMAssco.add(objUMA);
                }
                system.debug('lstUMAssco----------'+lstUMAssco);
                insert lstUMAssco;
            }
            catch(System.CalloutException e){
                System.debug('Callout error: '+ e);
            }
        }
        //assigning message based on Roles and Subordinates selection
        else if(boolR_S){
            try{
                system.debug('Save R_S---------'+strR_S);
                setGlobalLst.add(strR_S);
                Set<String> strRoleId = new Set<String>();
                Set<String> setCurrentRoleId = new Set<String>();
                
                //List<UserRole> strRoleId = [Select Id from UserRole where ParentRoleId =:strR_S];
                for(UserRole objUR : [Select Id, ParentRoleId from UserRole where ParentRoleId =:strR_S]){
                    system.debug('strRoleId---------'+objUR.id);
                    strRoleId.add(objUR.id);
                    setGlobalLst.add(objUR.id);
                }
                
                if(strRoleId.size() > 0) {
                    List<UserRole> lstRole = new List<UserRole>();
                    do{
                        lstRole = new List<UserRole>();
                        for(UserRole objUR1 : [Select Id, ParentRoleId from UserRole where ParentRoleId =:strRoleId]) {
                            setCurrentRoleId.add(objUR1.Id);
                            lstRole.add(objUR1);
                            setGlobalLst.add(objUR1.id);
                        }
                        strRoleId = new Set<String>();
                        strRoleId.addAll(setCurrentRoleId);
                        setCurrentRoleId = new Set<String>();
                    }while(lstRole.size() > 0);
                }
                system.debug('setGlobalLst---------'+setGlobalLst);
                
                lstGlobalUser = new List<User>();
                lstGlobalUser = [select Id from User where UserRole.id  IN :setGlobalLst];
                
                List<HF_User_Messages_Association__c> lstUMAssco = new List<HF_User_Messages_Association__c>();
                
                for(User objU : lstGlobalUser){
                    //lstUMAssco.add(new HF_User_Messages_Association__c(HF_User__c = objU.id, HF_Broadcast_Message__c = strMessageId));
                    HF_User_Messages_Association__c objUMA = new HF_User_Messages_Association__c();
                    objUMA.HF_User__c = objU.id;
                    objUMA.HF_Broadcast_Message__c = strMessageId;
                    lstUMAssco.add(objUMA);
                }
                system.debug('lstUMAssco----------'+lstUMAssco);
                insert lstUMAssco;
            }
            catch(System.CalloutException e){
                System.debug('Callout error: '+ e);
            }
        }
        //assigning message to all Users
        else if(boolUsers){
            try{
                system.debug('Save Users---------');
                lstAllUser = [SELECT Id, EmployeeNumber, IsActive, firstname, lastname, Username FROM User];
                system.debug('lstAllUser---------'+lstAllUser);
                
                List<HF_User_Messages_Association__c> lstUMAssco = new List<HF_User_Messages_Association__c>();
                for(User objU : lstAllUser){
                    //lstUMAssco.add(new HF_User_Messages_Association__c(HF_User__c = objU.id, HF_Broadcast_Message__c = strMessageId));
                    HF_User_Messages_Association__c objUMA = new HF_User_Messages_Association__c();
                    objUMA.HF_User__c = objU.id;
                    objUMA.HF_Broadcast_Message__c = strMessageId;
                    lstUMAssco.add(objUMA);
                }
                system.debug('lstUMAssco----------'+lstUMAssco);
                insert lstUMAssco;
            }
            catch(System.CalloutException e){
                System.debug('Callout error: '+ e);
            }
        }
        //assigning message to queues
        else if(boolQueue){
            try{
                system.debug('Save Queues---------');
                set<String> setQueue = new set<String>();
                set<String> setUMAQueue = new set<String>();
                
                for(GroupMember objGM: [select UserOrGroupId from GroupMember where GroupId =:strQueues]){
                    setQueue.add(objGM.UserOrGroupId);
                }
                for(HF_User_Messages_Association__c objUMAssoc: [select HF_User__c from HF_User_Messages_Association__c where HF_Broadcast_Message__c =:strMessageId]){
                    setUMAQueue.add(objUMAssoc.HF_User__c);
                }
                List<HF_User_Messages_Association__c> lstUMAssco_Queue = new List<HF_User_Messages_Association__c>();
                
                for(Id objU_Q : setQueue){
                    //lstUMAssco.add(new HF_User_Messages_Association__c(HF_User__c = objU.id, HF_Broadcast_Message__c = strMessageId));
                    
                    if(!setUMAQueue.contains(objU_Q)){
                        system.debug('record not present');
                        HF_User_Messages_Association__c objUMA_Q = new HF_User_Messages_Association__c();
                        objUMA_Q.HF_User__c = objU_Q;
                        objUMA_Q.HF_Broadcast_Message__c = strMessageId;
                        lstUMAssco_Queue.add(objUMA_Q);
                    }
                }
                system.debug('lstUMAssco_Queue----------'+lstUMAssco_Queue);
                if(lstUMAssco_Queue.size()>0){
                   insert lstUMAssco_Queue; 
                }
                
            }
            catch(System.CalloutException e){
                System.debug('Callout error: '+ e);
            }
        }
        //assigning message to public group
        else if(boolPublicGroup){
            try{
                system.debug('Save Public Group---------');
                set<String> setPublicGroup = new set<String>();
                set<String> setUMAPG = new set<String>();
                
                for(GroupMember objGM_PG: [select UserOrGroupId from GroupMember where GroupId =:strPublicGroup]){
                    setPublicGroup.add(objGM_PG.UserOrGroupId);
                }
                for(HF_User_Messages_Association__c objUMAssoc_PG: [select HF_User__c from HF_User_Messages_Association__c where HF_Broadcast_Message__c =:strMessageId]){
                    setUMAPG.add(objUMAssoc_PG.HF_User__c);
                }
                List<HF_User_Messages_Association__c> lstUMAssco_PG = new List<HF_User_Messages_Association__c>();
                
                for(Id objU_PG : setPublicGroup){
                    
                    if(!setUMAPG.contains(objU_PG)){
                        system.debug('record not present');
                        HF_User_Messages_Association__c objUMA_PG = new HF_User_Messages_Association__c();
                        objUMA_PG.HF_User__c = objU_PG;
                        objUMA_PG.HF_Broadcast_Message__c = strMessageId;
                        lstUMAssco_PG.add(objUMA_PG);
                    }
                }
                system.debug('lstUMAssco_PG----------'+lstUMAssco_PG);
                if(lstUMAssco_PG.size()>0){
                   insert lstUMAssco_PG; 
                }
                
                
            }
            catch(System.CalloutException e){
                System.debug('Callout error: '+ e);
            }
        }
    }
    
    //User selection for Ad Hoc message assignment
    public void GetSelected(){
        system.debug('strIdChoosen---------'+strIdChoosen);
        if(setSelectUser.contains(strIdChoosen)){
            setSelectUser.remove(strIdChoosen);
        }
        else{
           setSelectUser.add(strIdChoosen); 
        }
        system.debug('setSelectUser---------'+setSelectUser);
    }
}