public with sharing class Pagination {
    public static List<Contact> MemberSearchUpdatedStatic = new List<Contact>();
    public List<Contact> MemberDetailUpdatedStatic = new List<Contact>();
    public List<Contact> MemberPCPUpdatedStatic = new List<Contact>();

    public Pagination(MemberSearch controller) {
        //MemberSearchUpdatedStatic = MemberSearch.MemberValuesUpdated;
        //setCon = null;
    }
    
    public Pagination(MemberDetail controller) {

    }

    public Pagination(HF_ChangePCPController controller) {

    }

    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public ApexPages.StandardSetController setCon {
    
        get{
            if(MemberSearchUpdatedStatic != null){
                size = 2;
                //string queryString = 'Select Name, Type, BillingCity, BillingState, BillingCountry from ';
                setCon = new ApexPages.StandardSetController(MemberSearchUpdatedStatic);
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
                system.debug('### IF CONDITION ### ');
                system.debug('### noOfRecords : '+noOfRecords);
            }
            else 
            {
                size = 0;
                setCon = new ApexPages.StandardSetController([SELECT Id FROM Account WHERE Id='123456789012345']);
                setCon.setPageSize(size);
                noOfRecords = 0;
                system.debug('### ELSE CONDITION ### ');
                system.debug('### setCon: '+setCon);
            }
            return setCon;
        }set;
    }
     
    Public List<Account> getAccounts(){
        List<Account> accList = new List<Account>();
        for(Account a : (List<Account>)setCon.getRecords())
            accList.add(a);
        return accList;
    }
     
    public pageReference refresh() {
        setCon = null;
        getAccounts();
        setCon.setPageNumber(1);
        return null;
    }
     
    public Boolean hasNext {
        get {
            if(setCon != null) 
                return setCon.getHasNext();
           else 
               return null; 
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            if(setCon != null)
                return setCon.getHasPrevious();
            else 
               return null;
        }
        set;
    }
  
    public Integer pageNumber {
        get {
            if(setCon != null) 
                return setCon.getPageNumber();
            else 
               return null;
        }
        set;
    }
  
    public void first() {
        setCon.first();
    }
  
    public void last() {
        setCon.last();
    }
  
    public void previous() {
        setCon.previous();
    }
  
    public void next() {
        setCon.next();
    }
}