/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  10-Aug-2015
 * Description: This Class does below processing
 *              1. Search for member.
 *              2. Change PCP redirect
 *              3. Claims and Authorization fetxh.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            10-Aug-2015              Initial version.
 **************************************************************************************/
public with sharing class MemberSearch_Cloned{
    
    //Global Variables.
    public String strMemberId{get;set;}
    public String strMemberName{get;set;}  //kd
    public String strSSN{get;set;}     //kd
    public Date strMemberDOB{get;set;}  //kd
    public String strPhone{get;set;}   //kd
    public String strCompany{get;set;}    //kd
    public String strGender{get;set;}  //kd
    public String strBenefitPackage{get;set;}  //kd
    public String strZip{get;set;}   //kd
    public String strSSNF{get;set;}  //kd
    public String strStatus{get;set;}   //kd
    public String strEmail{get;set;}    //kd
    
    public Boolean boolAdvSearch{get;set;} //kd
    public Boolean boolShowClaims{get;set;}
    public Boolean boolShowMsg{get;set;}
    public Boolean boolShowUpdate{get;set;}
    public Boolean boolShowInfo{get;set;}    
    public Boolean boolShowClear{get;set;} 
    public String strDBSelect{get;set;}
    
    public String strHttpURL;
    public Boolean boolShowCriteriaNullMessage {get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    public List<MemberJSON> MemberValuesUpdated{get;set;}
    public List<MemberJSON> MemberValues{get;set;}
    Public MemberJSON memberInfo {get; set;} 
    public Integer recordsReceived{get;set;}
    public String strSearchedMemsJSON;
    
    String strResBody;
    
    public MemberSearch_Cloned(){    
       boolShowClaims = TRUE;       
       boolShowUpdate = FALSE; 
       boolShowInfo = FALSE;
     
       
       
       /************checking for member JSON *****************/
        /*if(ApexPages.CurrentPage().getParameters().containsKey('strSearchedMemsJSON') && String.isNotBlank(ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON'))){
            //strSearchedMemsJSON = (mapPageParams.containsKey('strSearchedMemsJSON'))? mapPageParams.get('strSearchedMemsJSON') : '';
            strSearchedMemsJSON = ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON');
            //MemberValuesUpdated = new List<MemberJSON>();
            MemberValuesUpdated = (List<MemberJSON>)JSON.deSerialize(strSearchedMemsJSON,List<MemberJSON>.class);
            system.debug('### listMembersSearched.size: '+MemberValuesUpdated);
            //listMembersSearched = new MemberJSON();
            //listMembersSearched = con[0];
            boolShowInfo = TRUE;
        }*/
    }
    
    
    /******************************
    ### method for fetching memeber from external db
    */
    public void searchButtonClicked() {
    	  boolAdvSearch=TRUE; //kd
        Long strDate = DateTime.now().getTime();
        
        system.debug('Start Time on member----->'+strDate);
        MemberValuesUpdated = new List<MemberJSON>();
       
       /******Checking for member id *******/
        if(!String.isBlank(strMemberId)){
            //creating http URL
            boolShowClaims = TRUE;    
            //strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/members/';
            
            strHttpURL = 'https://hf-eai-oauthprovider-dev.cloudhub.io/access-token';
            
            
            system.debug('The Endpoint URL is-->'+strHttpURL);
        }
        else{
            /******Search field left empty*******/
            boolShowUpdate = TRUE;
            boolShowCriteriaNullMessage = TRUE;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please enter Member Id');
            ApexPages.addMessage(myMsg);
        }         
           
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if(!String.isBlank(strHttpURL)){
            //req.setEndpoint(strHttpURL);
            //req.setMethod('GET');
            //req.setTimeOut(120000);
            
            
            req.setHeader('grant_type', 'client_credentials');
            req.setHeader('scope', 'WRITE');
            req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
            req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
            req.setEndpoint(strHttpURL);
            req.setMethod('GET');
            
        
            try {
                /******making callout to external db*******/
                res = http.send(req);
                System.debug('-----------------'+res);
                System.debug('---------------+STATUS:'+res.getBody());
                system.debug('THE RES-->'+res.getBody());
                strResBody = res.getBody();
                
                if(!String.isBlank(strResBody) && strResBody != null && (!strResBody.contains('NullPayload') || !strResBody.contains('Resource not found') || !strResBody.contains('Gateway Time-out')))
                {
                    system.debug('Body After Removal-->'+strResBody);
                
                    Response strNewResponse = (Response)JSON.deSerialize(strResBody,Response.class);
                    
                    
                    system.debug('strNewResponse.access_token-->'+strNewResponse.access_token);
                    
                    strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/members/';
                    
                    strHttpURL += (strMemberId != null && strMemberId != '') ? +strMemberId : '';
                    
                    if(!String.isBlank(strHttpURL))
                    {
                        req = new HttpRequest();
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('Authorization', 'Bearer '+strNewResponse.access_token);
                        req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
                        req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
                        req.setEndpoint(strHttpURL);
                        req.setMethod('GET');
                        //req.setTimeOut(120000);
                        //req.setBody('{"memberNo": "5000759W5","companyNo": "01","pcpNo": "106566-A26","reasonCode": "M816","actionCode": "E14P","pcpSeenToday": "No"}');
                        res = http.send(req);
                        System.debug('-----------------'+res);
                        System.debug('---------------+STATUS:'+res.getBody());
                        strResBody = res.getBody();
                        
                        if(!String.isBlank(strResBody) && strResBody != null && !strResBody.contains('NullPayload'))
                        {
                            
                          for(MemberJSON objMember : (List<MemberJSON>)JSON.deSerialize(strResBody,List<MemberJSON>.class) )
                           {     
                                 if(objMember.BirthDate!=null && objMember.BirthDate!=''){
                                    String strBirthDate = objMember.BirthDate;
                                    objMember.BirthDate = strBirthDate.substring(3,5)+'-'+strBirthDate.substring(5,7)+'-'+strBirthDate.substring(0,1)+(strBirthDate.substring(0,1) == '2' ? '0' : '9')+strBirthDate.substring(1,3);
                                 }
                                 
                                 MemberValuesUpdated.add(objMember);
                           }
                            
                            if(MemberValuesUpdated!= null && MemberValuesUpdated.isEmpty()){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No records to display.');
                                ApexPages.addMessage(myMsg);
                                boolShowUpdate = TRUE;
                                boolShowClear = TRUE;
                            }
                            else{
                                boolShowUpdate = FALSE;
                                boolShowInfo = TRUE;
                                boolShowClear = TRUE;
                                recordsReceived = MemberValuesUpdated.size();
                                long strNewDate = DateTime.now().getTime() - strDate;
                                system.debug('End Time on member------>'+DateTime.now().getTime());
                                
                                system.debug('TimeStamp on member-------------->'+strNewDate);  
                                system.debug('MemberValuesUpdated-------------->'+MemberValuesUpdated);    
                            }
                            
                        }
                        else{
                            /******no records retrived from callout*******/
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Data available.');
                            ApexPages.addMessage(myMsg);
                            boolShowUpdate = TRUE;
                            boolShowClear = TRUE;
                        }
                    }    
                    
                }
                
                
                /******checking for response*******
                if(!String.isBlank(strResBody) && strResBody != null && !strResBody.contains('NullPayload'))
                {
                    
                  for(MemberJSON objMember : (List<MemberJSON>)JSON.deSerialize(strResBody,List<MemberJSON>.class) )
                   {     
                         if(objMember.BirthDate!=null && objMember.BirthDate!=''){
                            String strBirthDate = objMember.BirthDate;
                            objMember.BirthDate = strBirthDate.substring(3,5)+'-'+strBirthDate.substring(5,7)+'-'+strBirthDate.substring(0,1)+(strBirthDate.substring(0,1) == '2' ? '0' : '9')+strBirthDate.substring(1,3);
                         }
                         
                         MemberValuesUpdated.add(objMember);
                   }
                    
                    if(MemberValuesUpdated!= null && MemberValuesUpdated.isEmpty()){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No records to display.');
                        ApexPages.addMessage(myMsg);
                        boolShowUpdate = TRUE;
                        boolShowClear = TRUE;
                    }
                    else{
                        boolShowUpdate = FALSE;
                        boolShowInfo = TRUE;
                        boolShowClear = TRUE;
                        recordsReceived = MemberValuesUpdated.size();
                        system.debug('MemberValuesUpdated-------------->'+MemberValuesUpdated);    
                    }
                    
                }
                else{
                    /******no records retrived from callout*******
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Data available.');
                    ApexPages.addMessage(myMsg);
                    boolShowUpdate = TRUE;
                    boolShowClear = TRUE;
                }*/
            }
            catch(System.CalloutException e) {
                boolShowResultNullMessage = TRUE;
                boolShowUpdate = TRUE;
                boolShowClear = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Oops! An Error Occurred. Please try again later!');
                strMemberId = null;
                ApexPages.addMessage(myMsg); 
                long strNewDate = DateTime.now().getTime() - strDate;
                system.debug('End Time-->'+DateTime.now().getTime());
                
                system.debug('TimeStamp on call fail-------------->'+strNewDate); 
                system.debug('Oops! An Error Occurred! in catch');
                System.debug('Callout error: '+ e);
                System.debug(res.toString());
            }
         }
         else{
             system.debug('Oops! An Error Occurred!');
         }
         system.debug('MemberValuesUpdated-------------->'+MemberValuesUpdated);
         
    }
    
    /******************************
    ### method for clearing search field for member id
    */
    public void ClearMember(){
       boolShowUpdate = False;
       MemberValuesUpdated = new List<MemberJSON>();
       strMemberId = null;
       boolShowClear = FALSE;
       boolShowInfo = False;
    }
       
    /******************************
    ### traversing to Member detail page, maintaing member reference   
    */
    public PageReference LinkClicked(){
        PageReference objRefToMemDtl;
        
        objRefToMemDtl = Page.Member_Detail_Page_Cloned;
        system.debug('### objRefToMemDtl: '+objRefToMemDtl);
        system.debug('### MemberValuesUpdated[0].strMemberId: '+MemberValuesUpdated[0].memberId);
        system.debug('### JSON Response: '+strResBody);
        if(!String.isBlank(strResBody) && strResBody != null){
            objRefToMemDtl.getParameters().put('strSearchedMemsJSON',strResBody);
        }
        else{
            objRefToMemDtl.getParameters().put('strSearchedMemsJSON',strSearchedMemsJSON);
        }
        
        objRefToMemDtl.getParameters().put('strMemberId',MemberValuesUpdated[0].memberId);
        //objRefToMemDtl.getParameters().put('strSearchedMemsJSON',strResBody);
        objRefToMemDtl.setRedirect(true);
        return objRefToMemDtl;
    }
    
    /******************************
    ### wrapper class for Member
    */
    public class MemberJSON{
        public String PlanEffectiveDate{get;set;}
        public String HOMPH2{get;set;}
        public String MemberID{get;set;}
        public String WRKEXT{get;set;}
        public String MemberExpirationDate{get;set;}
        public String SubscriberID{get;set;}
        public String WRKPH1{get;set;}
        public String GroupName{get;set;}
        public String BirthDate{get;set;}
        public String CellPhone{get;set;}
        public String LOB{get;set;}
        public String CompanyNumber{get;set;}
        public String HOMPH1{get;set;}
        public String EmailAddress{get;set;}
        public String City{get;set;}
        public String GroupNumber{get;set;}
        public String Gender{get;set;}
        public String WRKPH2{get;set;}
        public String AddressLine2{get;set;}
        public String ZipCode{get;set;}
        public String OtherPhone{get;set;}
        public String HOMPH3{get;set;}
        public String AddressLine1 {get;set;}
        public String BenefitPackage{get;set;}
        public String WRKPH3{get;set;}
        public String State{get;set;}
        public String PlanExpirationDate{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        
    }
    
    /******************************
    ### wrapper class for response
    */
    public class Response{
        public String access_token{get;set;}   
        public String scope{get;set;}
        public String token_type{get;set;}
        public String expires_in{get;set;}
    }
}