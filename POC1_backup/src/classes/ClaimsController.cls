/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  26/01/2015
 * Description: This Class does below processing
 *              1. Processes the Claims functionality.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Raymundo Rodriguez        26/01/2015              Initial version.
 **************************************************************************************/
public with sharing class ClaimsController {
    
    //Global Variables.
    public transient List<ClaimWrapper> claims {get; set;}
    private static final Integer MAX_NUMBER_OF_CLAIMS = 10;
    
    /*
    * Method name  :   ClaimsController
    * Description  :   Constructor of the class.
    * Return Type  :   -
    * Parameter    :   -
    */
    public ClaimsController() {
        fetchClaimRecords();
    }
    
    /*
    * Method name  :   fetchClaimRecords
    * Description  :   Queries existing Claim records owned by the current user.
    * Return Type  :   Void.
    * Parameter    :   -
    */
    public void fetchClaimRecords() {
        String query = 'SELECT Id, Amount_Billed__c, Contact__r.Name, Date_Visited__c, Member_Policy__c, ' +
                'Processed_Date__c, Provider__c, Responsibility__c, Status__c, Name ' + 
                'FROM Claim__c ORDER BY CreatedDate DESC LIMIT ' + MAX_NUMBER_OF_CLAIMS;
        claims = new List<ClaimWrapper>();
        for(Claim__c claimRecord : DataBase.query(query)) {
            ClaimWrapper newClaim = new ClaimWrapper(claimRecord);
            claims.add(newClaim);
        }
    }
    
    /*
    * Method name  :   ClaimWrapper
    * Description  :   Wrapper Class for Claim records.
    * Return Type  :   -
    * Parameter    :   -
    */
    public class ClaimWrapper {
        
        //Global variables.
        public Id claimId {get; set;}
        public Decimal claimAmountBilled {get; set;}
        public Decimal claimResponsibility {get; set;}
        public Decimal claimPlanPaidPercentage {get; set;}
        public Decimal claimPlanPaidDecimal {get; set;}
        public Decimal claimYourResponsibilityPercentage {get; set;}
        public Decimal claimYourResponsibilityDecimal {get; set;}
        public String contactName {get; set;}
        public String claimNumber {get; set;}
        public String claimDateVisitedFormatted {get; set;}
        public String claimProcessedDateFormatted {get; set;}
        public String claimDateReceivedFormatted {get; set;}
        public String claimProvider {get; set;}
        public String claimStatus {get; set;}
        public String claimStatusExplanation {get; set;}
        public String claimResponsabilityColor {get; set;}
        public String providerAddress {get; set;}
        public Date claimDateVisited {get; set;}
        public Date claimProcessedDate {get; set;}
        public Date claimDateReceived {get; set;}
        public List<ClaimDetailWrapper> claimDetailRecords {get; set;}
        
        
        /*
        * Method name  :   ClaimWrapper
        * Description  :   Constructor of the class.
        * Return Type  :   -
        * Parameter    :   Claim record.
        */
        public ClaimWrapper(Claim__c claimRecord) {
            if(claimRecord != null) {
                claimDetailRecords = new List<ClaimDetailWrapper>();
                
                //Beginning of Hardcoded data.--------------------------
                claimDateReceived = System.today();
                providerAddress = 'Steadman Hawkins Clinic - Dallas<br></br>123 Main Street<br></br>Suite 600<br></br>Dallas, TX 75205';
                claimStatusExplanation = 'Your claim has been received and is currently being reviewed by our claims team.';
                claimDetailRecords.add(new ClaimDetailWrapper(claimRecord, 195.00, 'Doctor\'s Office Visit, Short CPT: 6587A'));
                claimDetailRecords.add(new ClaimDetailWrapper(claimRecord, 57.00, 'Exercise Therapy CPT: G7984'));
                claimDetailRecords.add(new ClaimDetailWrapper(claimRecord, 45.00, 'Body Massage Therapy CPT: G2034'));
                //End of hardcoded data.--------------------------------
                
                claimId = claimRecord.Id;
                claimNumber = claimRecord.Name;
                claimAmountBilled = claimRecord.Amount_Billed__c;
                claimResponsibility = claimRecord.Responsibility__c;
                claimProvider = claimRecord.Provider__c;
                claimStatus = claimRecord.Status__c;
                claimDateVisited = claimRecord.Date_Visited__c;
                if(claimDateVisited != null) {
                    claimDateVisitedFormatted = claimDateVisited.format();
                }
                claimProcessedDate = claimRecord.Processed_Date__c;
                if(claimProcessedDate != null) {
                    claimProcessedDateFormatted = claimProcessedDate.format();
                }
                if(claimRecord.Contact__r != null) {
                    contactName = claimRecord.Contact__r.Name;
                }
                if(claimDateReceived != null) {
                    claimDateReceivedFormatted = claimDateReceived.format();
                }
                
                //Calculating percentages.
                Decimal total = 0.0;
                claimPlanPaidDecimal = 0.0;
                claimYourResponsibilityDecimal = 0.0;
                for(ClaimDetailWrapper claimDetailRecord : claimDetailRecords) {
                    claimPlanPaidDecimal += claimDetailRecord.claimDetailPlanPaid;
                    claimYourResponsibilityDecimal += claimDetailRecord.claimDetailYourResponsibility;
                }
                total = claimPlanPaidDecimal + claimYourResponsibilityDecimal;
                claimPlanPaidPercentage = (claimPlanPaidDecimal * 100) / total;
                claimYourResponsibilityPercentage = (claimYourResponsibilityDecimal * 100) / total;
                if(claimResponsibility > 0) {
                    claimResponsabilityColor = 'color: red;';
                }
            }
        }
    }
    
    /*
    * Method name  :   ClaimDetailWrapper
    * Description  :   Wrapper Class for Claim Detail records.
    * Return Type  :   -
    * Parameter    :   -
    */
    public class ClaimDetailWrapper {
        
        //Global variables.
        public Id claimDetailId {get; set;}
        public Decimal claimDetailPlanPaid {get; set;}
        public Decimal claimDetailYourResponsibility {get; set;}
        public String claimDetailName {get; set;}
        public String claimDetailDateFormatted {get; set;}
        public Date claimDetailDate {get; set;}
        
        /*
        * Method name  :   ClaimDetailWrapper
        * Description  :   Constructor of the class.
        * Return Type  :   -
        * Parameter    :   Claim record.
        */
        public ClaimDetailWrapper(Claim__c claimRecord, Decimal planPaid, String title) {
            if(claimRecord != null) {
                
                //Beginning of Hardcoded data.--------------------------
                claimDetailDate = System.today();
                if(claimDetailDate != null) {
                    claimDetailDateFormatted = claimDetailDate.format();
                }
                claimDetailName = title;
                claimDetailPlanPaid = planPaid;
                claimDetailYourResponsibility = 0.00;
                //End of hardcoded data.--------------------------------
            }
        }
    }
}