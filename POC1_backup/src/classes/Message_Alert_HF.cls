/* Create By:   Deloitte Consulting LLP.
 * CreateDate:  08-Oct-2015
 * Description: This Class processes the alerts to be shown for the member on the Member 360 page.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rashi Joshi           08-Oct-2015              Initial version.
 **************************************************************************************/

public class Message_Alert_HF{

public boolean boolDisplayPopup {get; set;}  
public List<HF_Broadcast_Message__c> lstMessageAlert{get;set;}
public HF_Broadcast_Message__c objMessage{get;set;}
public string strMessage{get;set;}

    //Constructor
    public Message_Alert_HF(){
        boolDisplayPopup = true; 
        defaultAlertList();        

    }
    
    public void defaultAlertList(){
        lstMessageAlert = new List<HF_Broadcast_Message__c>();
        lstMessageAlert = [Select Id, HF_Broadcast_Message_Name__c, HF_Start_Date__c, HF_End_Date__c, HF_Message__c, HF_Resolvable__c FROM HF_Broadcast_Message__c where Recordtype.DeveloperName =: System.Label.HF_MessageAlert_RecType_DevName];
        
        system.debug('lstMessageAlert-------->'+lstMessageAlert);
    }
    
    public void closePopup() {        
        boolDisplayPopup = false;    
    } 
        
    public void showPopup() {        
        boolDisplayPopup = true;    
    }
    
    public void gotoMessage(){
      /*objMessage = new HF_Broadcast_Message__c();
      objMessage = [select id,HF_Broadcast_Message_Name__c from HF_Broadcast_Message__c where id =:strMessage];
      system.debug('objMessage------------>'+objMessage);
      window.location.href = '/'+objMessage.HF_Broadcast_Message_Name__c;*/
      }
    
}