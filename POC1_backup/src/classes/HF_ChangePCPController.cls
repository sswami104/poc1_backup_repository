/*************************
### Created By: Srikanth Kumar Gudala (srgudala@deloitte.com)
### Created On: 10-Aug-15
### Class Name: HF_ChangePCPController
### Where is this used: HF_ChangePCP VF page
*/
public with sharing class HF_ChangePCPController {
    public String strProviderNum{get;set;}
    public String strFirstName{get;set;}
    public String strZIPCode{get;set;}
    public String strLastName{get;set;}
    public String strCity{get;set;}
    public String strAgeAccept{get;set;}
    public String strReasonCode{get;set;}
    public String strActionCode{get;set;}
    public String strPCPSeen{get;set;}
    public String strCompanyNumber{get;set;}
    
    public String strState{get;set;}
    public String strCompany{get;set;}
    public String strEffectiveDate{get;set;}
    public String strMemberSelected{get;set{system.debug('### strMemberSelected: '+strMemberSelected);strMemberSelected=value;}}
    public List<Member> listMembers{get;set;}
    public Boolean boolShowCriteriaNullMessage {get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    public List<Provider__c> ProviderValues{get;set;}
    public List<Provider__c> ProviderValuesUpdated{get;set;}
    public Contact dummyContact{get;set;}
    public Boolean boolShowUpdate{get;set;}
    public Boolean boolShowPCP{get;set;}
    public Boolean boolShowPCPList{get;set;}
    public String strUpdateProvider{get;set;}
    public MemberJSON listMembersSearched{get; set;}
    
    public String strHttpURL;
    private String strMemberId;
    private Map<String,String> mapPageParams;
    public String strSearchedMemsJSON;
    
    public HF_ChangePCPController(){
        boolShowUpdate = FALSE;
        boolShowPCP = FALSE;
        boolShowPCPList= FALSE;
        dummyContact = new Contact();
        mapPageParams = new Map<String,String>();
        mapPageParams = ApexPages.currentPage().getParameters();
        strMemberId = (mapPageParams.containsKey('id'))? mapPageParams.get('id') : '';
        if(ApexPages.CurrentPage().getParameters().containsKey('strSearchedMemsJSON') && String.isNotBlank(ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON'))){
            //strSearchedMemsJSON = (mapPageParams.containsKey('strSearchedMemsJSON'))? mapPageParams.get('strSearchedMemsJSON') : '';
            strSearchedMemsJSON = ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON');
            list<MemberJSON> con = new List<MemberJSON>();
            con = (List<MemberJSON>)JSON.deSerialize(strSearchedMemsJSON,List<MemberJSON>.class);
            system.debug('### listMembersSearched.size: '+listMembersSearched);
            listMembersSearched = new MemberJSON();
            listMembersSearched = con[0];
        }
        
    }
    public void testSelected(){
        system.debug('### strMemberSelected: '+strMemberSelected);
    }
    /**************************
    ### Runs when Search button is clicked
    ### Searched in external DB for the data
    */
    public void searchButtonClicked(){
        List<Contact> listContacts;
        Integer intMemberCounter;
        ProviderValues = new List<Provider__c>();
        ProviderValuesUpdated = new List<Provider__c>();
        
        ProviderValues = [Select Id, First_Name__c, Last_Name__c, Provider_Id__c, Address__c, Expiry_Date__c from Provider__c where Provider_Id__c =:strProviderNum]; 
                   
        for(Provider__c objProvider : ProviderValues) 
        {                         
             ProviderValuesUpdated.add(objProvider);
        }
         boolShowPCP = TRUE;
         boolShowPCPList = TRUE;
                    
    }
    /*************************
    ### Runs when Update PCP button is clicked
    ### posts updated PCP details to external DB
    */
    public PageReference UpdatePCP(){
        boolShowUpdate = FALSE;
        system.debug('############################');
        system.debug('### Provider: '+ApexPages.currentPage().getParameters().get('strMemberSelected'));
        system.debug('### Provider Updated::::::::::::: '+strUpdateProvider);
        if(!String.isBlank(strUpdateProvider)&&!String.isBlank(strReasonCode)&&!String.isBlank(strActionCode)&&!String.isBlank(strCompanyNumber)&&!String.isBlank(strPCPSeen))
        {
            system.debug('############################?????????????????????????');
            //boolShowClaims = TRUE;    
            //strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/access-token';  
            strHttpURL = 'https://hf-eai-oauthprovider-dev.cloudhub.io/access-token';
            system.debug('The Endpoint URL is-->'+strHttpURL);
        }
        else
        {
            system.debug('?????????????????????????############################');
            boolShowCriteriaNullMessage = TRUE;
            boolShowUpdate = TRUE;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please select Provider or enter values to Action Code, Company Number, Reason Code, Last Seen. ');
            ApexPages.addMessage(myMsg);
            return null;
        }
            
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if(!String.isBlank(strHttpURL))
        {
            req.setHeader('grant_type', 'client_credentials');
            req.setHeader('scope', 'WRITE');
            //req.setHeader('scope', 'all');
            req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
            req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
            req.setEndpoint(strHttpURL);
            req.setMethod('GET');
            //req.setTimeOut(120000);
        
            try 
            {
                system.debug('THE REQ-->'+req.getBody());
                res = http.send(req);
                
                //sessionCookie = res.getHeader('Set-Cookie');
                //system.debug('sessionCookie------------->'+sessionCookie);
                
                system.debug('THE RES-->'+res.getBody());
                String strResBody = res.getBody();
            
                if(!String.isBlank(strResBody) && strResBody != null && (!strResBody.contains('NullPayload') || !strResBody.contains('Resource not found') || !strResBody.contains('Gateway Time-out')))
                {
                    system.debug('Body After Removal-->'+strResBody);
                
                    Response strNewResponse = (Response)JSON.deSerialize(strResBody,Response.class);
                    
                    
                    system.debug('strNewResponse.access_token-->'+strNewResponse.access_token);
                    
                    //strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/pcp?client_id=9b95c64f71e34399b874d63af2ff031f&client_secret=4a5a56f64b474adcA86CBB58D0615406';
                    strHttpURL = 'https://hf-eai-pcp-proxy-dev.cloudhub.io/pcp';
                    
                    if(!String.isBlank(strHttpURL))
                    {
                        string strBody;
                        req = new HttpRequest();
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('Authorization', 'Bearer '+strNewResponse.access_token);
                        req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
                        req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
                        req.setEndpoint(strHttpURL);
                        req.setMethod('PUT');
                        //req.setTimeOut(120000);
                        strBody = '{\"member_number\":\"';
                        strBody += listMembersSearched.MemberID+'\"';
                        strBody += ',\"copmany_number\":\"';
                        strBody += strCompanyNumber+'\"';
                        strBody += ',\"pcp_number\":\"';
                        strBody += strUpdateProvider+'\"';
                        strBody += ',\"reason_code\":\"';
                        strBody += strReasonCode+'\"';
                        strBody += ',\"action_code\":\"';
                        strBody += strActionCode+'\"';
                        strBody += ',\"is_pcp_seen_today\":\"';
                        strBody += strPCPSeen+'\"';
                        strBody += '}';
                        System.debug('----------------strBoby--------------'+strBody);
                        //req.setBody('{"memberNo": "5000759W5","companyNo": "01","pcpNo": "106566-A26","reasonCode": "M816","actionCode": "E14P","pcpSeenToday": "No"}');
                        req.setBody(strBody);
                        res = http.send(req);
                        System.debug('-----------------'+res);
                        System.debug('---------------+STATUS:'+res.getBody());
                        
                        boolShowUpdate = TRUE;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Provider Updated Successfully!');
                        ApexPages.addMessage(myMsg);
                        strUpdateProvider = null;
                        return null;
                    }    
                    return null;
                }
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Target System busy, Try again later!');
                ApexPages.addMessage(myMsg);
                return null;
            }
                 
            catch(System.CalloutException e) 
            {
                boolShowResultNullMessage = TRUE;
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Oops! An Error Occurred.');
                ApexPages.addMessage(myMsg);   
                system.debug('Oops! An Error Occurred!');
                System.debug('Callout error: '+ e);
                System.debug(res.toString());
                return null;
            }
         }
         else
         {
             system.debug('Oops! An Error Occurred!');
             return null;
         }
        //### do callout to update PCP
    }
    /******************************
    ### parse search results JSON
    */
    
    public void clearPCP(){
        boolShowUpdate = FALSE;
        strActionCode = null;
        strCompanyNumber = null;
        strPCPSeen = null;
        strReasonCode = null;
        ProviderValuesUpdated = new List<Provider__c>() ;
    }
    private void parseSearchResults(String pStrJSONResponse){
        listMembers = new List<Member>();
        
        //### Parse JSON response here
    }
    public PageReference backButtonClicked(){
        PageReference objRefToMemDtlPage;
        
        objRefToMemDtlPage = Page.Member_Detail_Page;
        objRefToMemDtlPage.getParameters().put('id',strMemberId);
        objRefToMemDtlPage.getParameters().put('strSearchedMemsJSON',strSearchedMemsJSON);
        objRefToMemDtlPage.setRedirect(true);
        return objRefToMemDtlPage;
    }
    public void radioBtnClicked(){
        system.debug('### strMemberSelected: '+strMemberId);
        system.debug('### Provider: '+ApexPages.currentPage().getParameters().get('strMemberSelected'));
        strUpdateProvider = ApexPages.currentPage().getParameters().get('strMemberSelected');
    }
    public class Member{
        public String strMemberId{get;set;}
        public String strMemberName{get;set;}
        public String strFirstName{get;set;}
        public String strLastName{get;set;}
        public String strPCPId{get;set;}
        public String strPCPName{get;set;}
        public String strAddress{get;set;}
        public String strExpiryDate{get;set;}
        public Integer intMemberCounter{get;set;}
        public Member(String pStrMemberId,String pStrMemberName,String pStrFirstName,String pStrLastName,
            String pStrPCPId,String pStrPCPName,String pStrAddress,String pStrExpiryDate,
            Integer pIntMemberCounter){
            strMemberId = pStrMemberId;
            strMemberName = pStrMemberName;
            strFirstName = pStrFirstName;
            strLastName = pStrLastName;
            strPCPId = pStrPCPId;
            strPCPName = pStrPCPName;
            strAddress = pStrAddress;
            strExpiryDate = pStrExpiryDate;
            intMemberCounter = pIntMemberCounter;
        }
    }
    
    public class Response{
        public String access_token{get;set;}   
        public String scope{get;set;}
        public String token_type{get;set;}
        public String expires_in{get;set;}
    }
    
    public class MemberJSON{
        public String PlanEffectiveDate{get;set;}
        public String HOMPH2{get;set;}
        public String MemberID{get;set;}
        public String WRKEXT{get;set;}
        public String MemberExpirationDate{get;set;}
        public String SubscriberID{get;set;}
        public String WRKPH1{get;set;}
        public String GroupName{get;set;}
        public String BirthDate{get;set;}
        public String CellPhone{get;set;}
        public String LOB{get;set;}
        public String CompanyNumber{get;set;}
        public String HOMPH1{get;set;}
        public String EmailAddress{get;set;}
        public String City{get;set;}
        public String GroupNumber{get;set;}
        public String Gender{get;set;}
        public String WRKPH2{get;set;}
        public String AddressLine2{get;set;}
        public String ZipCode{get;set;}
        public String OtherPhone{get;set;}
        public String HOMPH3{get;set;}
        public String AddressLine1 {get;set;}
        public String BenefitPackage{get;set;}
        public String WRKPH3{get;set;}
        public String State{get;set;}
        public String PlanExpirationDate{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        
    }
    
    
}