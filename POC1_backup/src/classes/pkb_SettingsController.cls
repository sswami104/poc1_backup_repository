public with sharing class pkb_SettingsController {

  /* ***** DEFAULT SETTINGS TO BE USED ELSEWHERE IN APP WHEN CUSTOM SETTINGS FOR CURRENT SITE CANNOT BE FOUND ***** */
  public final static Boolean DEFAULT_DISPLAY_SUMMARY = true;
  public final static Boolean DEFAULT_ACTIVATE_CONTACT_US = true;
  public final static Integer DEFAULT_RESULTS_SIZE = 10;
  public final static Integer DEFAULT_POPULAR_SIZE = 3;
  public final static Integer DEFAULT_RELATED_SIZE = 3;
  public final static Boolean DEFAULT_MULTI_LANGUAGE = false;
  public final static Boolean DEFAULT_CREATE_ACCOUNT_CONTACT = false;
  public final static String DEFAULT_LANGUAGE = 'en_US';

  
}