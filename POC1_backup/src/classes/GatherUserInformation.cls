/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  06/09/2014
 * Description: This Class does below processing
 *              1. Created Lead from FORM.
                2. Convert lead.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Saurabh                 06/09/2014              Initial version.
 *  * Saurabh                 09/09/2014              Added Social Interaction insertion. 
 **************************************************************************************/
public without sharing class GatherUserInformation {
    
    // VARIABLE
    public String leadName {get;set;}
    public String leadGender {get;set;}
    public String leadCounty {get;set;} 
    public String leadInterest {get;set;}   
    public String leadZipcode {get;set;}
    public String leadTobacco {get;set;}
    public Date leadDOB {get;set;}
    public Boolean firstScreen{get;set;}
    public Boolean secondScreen{get;set;}
    public Boolean thirdScreen{get;set;}
    public String bodyCssClass {get;set;}
    
    @TestVisible Lead leadObj;
    @TestVisible Boolean isConverted;           
    Social_Interaction__c socialObj;
    Id contactId;
    
    // CONSTRUCTOR
   /*
    public GatherUserInformation() {
        leadName = 'Stacy Morgan';
        leadGender = 'Female';
        leadZipcode = '75202';
        //leadDOB = Date.newInstance(1980,05,05);
        firstScreen = true;
        secondScreen = false;
        thirdScreen = false;
        isConverted = false;
        socialObj= new Social_Interaction__c();
        bodyCssClass = 'mc-welcome-page';
        
    }
    */
    
     public GatherUserInformation(ApexPages.StandardController Con) {
        leadName = 'Stacy Morgan';
        leadGender = 'Female';
        leadZipcode = '75202';
        //leadDOB = Date.newInstance(1980,05,05);
        firstScreen = false;
        secondScreen = true;
        thirdScreen = false;
        isConverted = false;
        socialObj= new Social_Interaction__c();
        bodyCssClass = 'mc-welcome-page';
        
    }
    
    
   /*
    * Method name  :   saveLeadInformation
    * Description  :   Create Lead.
    * Return Type  :   -
    * Parameter    :   -
    */
    public void saveLeadInformation() {
        System.debug(leadName);
        System.debug(leadGender);
        System.debug(leadCounty);   
        System.debug(leadInterest); 
        System.debug(leadZipcode);
        System.debug(leadTobacco);
        System.debug(leadDOB);
        
        
        String leadFirstName = null, leadLastName = null;
        Integer leadIndex = 0;
        try {
            
            if (leadName != null) {
                leadIndex = leadName.lastIndexOfIgnoreCase(' ');
                if (leadIndex == -1) {
                    leadLastName = leadName;
                }
                else {
                    leadFirstName = leadName.subString(0, leadIndex);
                    leadLastName = leadName.subString(leadIndex + 1, leadName.length());
                }
            }
            
            
            leadObj = new Lead ( FirstName = leadFirstName, LastName = leadLastName, Company = leadName, Gender__c = leadGender, Residential_County__c = leadCounty, Primary_Interest__c = leadInterest,
                                PostalCode = leadZipcode, Tobacco_Use__c = leadTobacco, Date_of_Birth__c = leadDOB, OwnerId = System.Label.CommunityUser);
                                
            Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Lead.fields.getMap();
            for( Lead_Default_Field_Values__c leadDefaultValue :[   select  Id, Field_API_Name__c,Field_Value__c
                                                                    from    Lead_Default_Field_Values__c
                                                                    limit   :Limits.getLimitQueryRows() - Limits.getQueryRows()  ] )
            {
                if( schemaFieldMap.containsKey( leadDefaultValue.Field_API_Name__c ) ) {
                    leadObj.put( leadDefaultValue.Field_API_Name__c, leadDefaultValue.Field_Value__c );
                }
            }                                                                   
            //leadObj.put('FirstName', 'Kelly');
            insert leadObj;
            
            
            // Created Social_Interaction
            socialObj = new Social_Interaction__c(RecordTypeID = System.Label.LeadRecordType, Related_Lead__c = leadObj.Id, OwnerId = System.label.Unassigned_Inbound_Lead);
            insert socialObj;
            
            firstScreen = false;
            secondScreen = true;
            thirdScreen = false;
            
            bodyCssClass = 'mc-compare-plans-page';            
        }
        catch (Exception excp) {
            System.debug('Exception in saveLeadInformation ::'+excp);
            firstScreen = true;
            secondScreen = false;
            thirdScreen = false;            
        }
                
    }

   /*
    * Method name  :   onSelectOfPlan
    * Description  :   Convert Lead.
    * Return Type  :   -
    * Parameter    :   -
    */  
    public void onSelectOfPlan() {
        try {
            if (!isConverted) {
                convertTheLead( leadObj );      
            }
            firstScreen = false;
            secondScreen = false;
            thirdScreen = true;
            bodyCssClass = 'mc-compare-plans-page';         
        } catch(Exception excp) {
            System.debug('Exception in onSelectOfPlan::'+excp);
            isConverted = false;
            firstScreen = false;
            secondScreen = true;
            thirdScreen = false;            
        }
        
    }


    /**
     * convertLeads
     * 
     * This method converts leads passed to it using the standard Database method "convertLead"
     * 
     * @param leadsToBeConverted This is a map of leads to be converted
     */
    private void convertTheLead( Lead leadToBeConverted ) {
        String convertedStatusMasterLabel;
        //Fetch the LeadStatus object for converted criteria
        convertedStatusMasterLabel 
            =   [   Select  MasterLabel
                    From    LeadStatus
                    Where   IsConverted = true 
                    limit   1 ].MasterLabel;
        
        //For the lead, create LeadConvert object
        Database.LeadConvert leadConvert = new Database.LeadConvert();
        leadConvert.setLeadId( leadToBeConverted.Id );            
        leadConvert.setConvertedStatus( convertedStatusMasterLabel );

        leadConvert.setOpportunityName( leadToBeConverted.Company );  
            
        //Using the Database.convertLead method convert the lead
        Database.LeadConvertResult leadConvertResult = Database.convertLead( leadConvert, false );
        if( leadConvertResult.success ) {
            isConverted = true;
            contactId = leadConvertResult.contactId;
            transferSocialInteractionRecords(leadToBeConverted.Id, contactId);
        }
        
    }


   /*
    * Method name  :   onViewOtherPlan
    * Description  :   Re-direct from 2.3 to 2.2
    * Return Type  :   -
    * Parameter    :   -
    */  
    public PageReference enrollRedirect() {
        PageReference pge = new PageReference('/apex/vf_EnrollmentForm?id='+contactId);
        pge.setRedirect( true );
        return pge;        
    }


   /*
    * Method name  :   onViewOtherPlan
    * Description  :   Re-direct from 2.3 to 2.2
    * Return Type  :   -
    * Parameter    :   -
    */  
    public void onViewOtherPlan() {
        firstScreen = false;
        secondScreen = true;
        thirdScreen = false;        
    }
    
    /*
    * Method name  :   transferSocialInteractionRecords
    * Description  :   Transfers all interaction records to Contact record.
    * Return Type  :   void
    * Parameter    :   -
    */  
    public void transferSocialInteractionRecords(Id leadId, Id contactId) {
        List<Social_Interaction__c> socialInteractions = [SELECT Id FROM Social_Interaction__c WHERE Related_Lead__c =: leadId];
        for(Social_Interaction__c record : socialInteractions) {
            record.Related_Lead__c  = null;
            record.Related_Contact__c = contactId;
            record.OwnerId = System.label.Unassigned_Inbound_Lead;
        }
        if(socialInteractions.size() > 0) {
            update socialInteractions;
        }
    }      
}