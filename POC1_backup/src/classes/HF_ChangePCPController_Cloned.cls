/*************************
### Created By: Rahul Saxena (rahulsaxena4@deloitte.com)
### Created On: 04-Sep-15
### Class Name: HF_ChangePCPController
### Where is this used: HF_ChangePCP_Cloned VF page
*/
public with sharing class HF_ChangePCPController_Cloned {
    public String strProviderNum{get;set;}
    /*public String strFirstName{get;set;}
    public String strZIPCode{get;set;}
    public String strLastName{get;set;}
    public String strCity{get;set;}*/
    public String strAgeAccept{get;set;}
    public String strReasonCode{get;set;}
    public String strActionCode{get;set;}
    public String strPCPSeen{get;set;}
    public String strCompanyNumber{get;set;}
    
    /*public String strState{get;set;}
    public String strCompany{get;set;}
    public String strEffectiveDate{get;set;}
    public String strMemberSelected{get;set{system.debug('### strMemberSelected: '+strMemberSelected);strMemberSelected=value;}}*/
    public Boolean boolShowCriteriaNullMessage {get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    public List<Provider__c> ProviderValues{get;set;}
    public List<Provider__c> ProviderValuesUpdated{get;set;}
    //public Contact dummyContact{get;set;}
    public Boolean boolShowUpdate{get;set;}
    public Boolean boolShowPCP{get;set;}
    public Boolean boolShowPCPList{get;set;}
    public Boolean boolShowPCPListResp{get;set;}
    public String strUpdateProvider{get;set;}
    public MemberJSON listMembersSearched{get; set;}
    public List<ProviderJSON> ProviderResp{get;set;}
    public List<ProviderJSON> UpdatedProviderResp{get;set;}
    
    public String strHttpURL;
    private String strMemberId;
    private Map<String,String> mapPageParams;
    public String strSearchedMemsJSON;
    
    public Boolean boolShowMsg{get;set;}
    public Integer recordsReceived{get;set;}
    
    public HF_ChangePCPController_Cloned (){
        boolShowUpdate = FALSE;
        boolShowPCP = FALSE;
        boolShowPCPList= FALSE;
        boolShowPCPListResp= FALSE;
        strActionCode = 'E14P';
        strCompanyNumber = '01';
        strPCPSeen = 'Yes';
        strReasonCode = 'M816';
        
        //dummyContact = new Contact();
        mapPageParams = new Map<String,String>();
        mapPageParams = ApexPages.currentPage().getParameters();
        strMemberId = (mapPageParams.containsKey('id'))? mapPageParams.get('id') : '';
        
        /************checking for member JSON *****************/
        if(ApexPages.CurrentPage().getParameters().containsKey('strSearchedMemsJSON') && String.isNotBlank(ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON'))){
            //strSearchedMemsJSON = (mapPageParams.containsKey('strSearchedMemsJSON'))? mapPageParams.get('strSearchedMemsJSON') : '';
            strSearchedMemsJSON = ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON');
            list<MemberJSON> con = new List<MemberJSON>();
            con = (List<MemberJSON>)JSON.deSerialize(strSearchedMemsJSON,List<MemberJSON>.class);
            system.debug('### listMembersSearched.size: '+listMembersSearched);
            listMembersSearched = new MemberJSON();
            listMembersSearched = con[0];
        }
        
    }
    
    ////////////code not in use
    /**************************
    ### Runs when Search button is clicked
    ### Searched in salesforce Provider object for the data
    */
    public void searchButtonClicked(){
        
        ProviderValues = new List<Provider__c>();
        ProviderValuesUpdated = new List<Provider__c>();
        
        if(strProviderNum!= null && strProviderNum!='')
        {
            ProviderValues = [Select Id, First_Name__c, Last_Name__c, Provider_Id__c, Address__c, Expiry_Date__c from Provider__c where Provider_Id__c =:strProviderNum]; 
            
            /************checking for provider available *****************/
            if(ProviderValues.size()!=0)
            {
                for(Provider__c objProvider : ProviderValues) 
                {                         
                     ProviderValuesUpdated.add(objProvider);
                }
                 boolShowPCP = TRUE;
                 boolShowPCPList = TRUE;
                 boolShowMsg = TRUE;
                 boolShowPCPListResp = FALSE;
                 recordsReceived = ProviderValuesUpdated.size();
            }
            else{
                /************Provider not found, check for provider id again*****************/
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please check for Provider Id. ');
                ApexPages.addMessage(myMsg);
            }
        }
        else{
            /************Provider id not provided while searching*****************/
            boolShowUpdate = TRUE;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please enter Provider Id. ');
            ApexPages.addMessage(myMsg);
        }
                    
    }////////////code not in use
    
    
    /*************************
    ### Runs when Update PCP button is clicked
    ### posts updated PCP details to external DB
    */
    public PageReference UpdatePCP(){
        Long strDate = DateTime.now().getTime();
        system.debug('Start Time on PCP----->'+strDate);
        
        UpdatedProviderResp = new List<ProviderJSON>();
        ProviderResp = new List<ProviderJSON>();
        boolShowUpdate = FALSE;
        system.debug('############################');
        system.debug('### Provider: '+ApexPages.currentPage().getParameters().get('strMemberSelected'));
        system.debug('### Provider Updated::::::::::::: '+strUpdateProvider);
        
        /************checking for member details available*****************/
        if(listMembersSearched!=null)
        {
            /************check for required input parameters available*****************/
            if(!String.isBlank(strProviderNum)&&!String.isBlank(strReasonCode)&&!String.isBlank(strActionCode)&&!String.isBlank(strCompanyNumber)&&!String.isBlank(strPCPSeen))
            {
                system.debug('############################?????????????????????????');
                /************building http URL*****************/
                strHttpURL = 'https://hf-eai-oauthprovider-dev.cloudhub.io/access-token';
                system.debug('The Endpoint URL is-->'+strHttpURL);
            }
            else
            {
                /******inputs missing, display error message*******/
                boolShowCriteriaNullMessage = TRUE;
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select Provider# or enter values to Action Code, Company Number, Reason Code, Last Seen. ');
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        else{
            /******member details not available*******/
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Member Details not available!');
            ApexPages.addMessage(myMsg);
            boolShowUpdate = TRUE;
            return null;
        }    
        
        /******creating URL for making http callout to external db*******/
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if(!String.isBlank(strHttpURL))
        {
            /******setting header inputs in the URL*******/
            req.setHeader('grant_type', 'client_credentials');
            req.setHeader('scope', 'WRITE');
            req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
            req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
            req.setEndpoint(strHttpURL);
            req.setMethod('GET');
            //req.setTimeOut(120000);
        
            try 
            {
                /******making callout to external db to get access token*******/
                res = http.send(req);
                
                System.debug('-----------------'+res);
                System.debug('-----------------Status_Code::::'+res.getStatusCode());
                System.debug('---------------+STATUS:'+res.getBody());
                system.debug('THE RES-->'+res.getBody());
                String strResBody = res.getBody();
                
                /******checking for response*******/
                if(!String.isBlank(strResBody) && strResBody != null && (!strResBody.contains('NullPayload') || !strResBody.contains('Resource not found') || !strResBody.contains('Gateway Time-out')))
                {
                    system.debug('Body After Removal-->'+strResBody);
                
                    Response strNewResponse = (Response)JSON.deSerialize(strResBody,Response.class);
                    
                    
                    system.debug('strNewResponse.access_token-->'+strNewResponse.access_token);
                    
                    strHttpURL = 'https://hf-eai-pcp-proxy-dev.cloudhub.io/pcp';
                    
                    if(!String.isBlank(strHttpURL))
                    {
                        /******setting header inputs in the URL*******/
                        string strBody;
                        req = new HttpRequest();
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('Authorization', 'Bearer '+strNewResponse.access_token);
                        req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
                        req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
                        req.setEndpoint(strHttpURL);
                        req.setMethod('PUT');
                        req.setTimeOut(120000);
                        
                        /******creating body for second callout*******/
                        strBody = '{\"member_number\":\"';
                        strBody += listMembersSearched.MemberID+'\"';
                        strBody += ',\"copmany_number\":\"';
                        strBody += strCompanyNumber+'\"';
                        strBody += ',\"pcp_number\":\"';
                        strBody += strProviderNum+'\"';
                        strBody += ',\"reason_code\":\"';
                        strBody += strReasonCode+'\"';
                        strBody += ',\"action_code\":\"';
                        strBody += strActionCode+'\"';
                        strBody += ',\"is_pcp_seen_today\":\"';
                        strBody += strPCPSeen+'\"';
                        strBody += '}';
                        strBody = strBody.replaceAll('(\\s+)', '');
                        System.debug('----------------strBoby--------------'+strBody);
                        
                        req.setBody(strBody);
                        res = http.send(req);
                        System.debug('-----------------'+res);
                        System.debug('---------------+STATUS_Update:'+res.getBody());
                        System.debug('---------------+STATUS:'+res.getStatusCode());
                        
                        //string strProviderResponse = res.getBody();
                        Integer strStatusCode = res.getStatusCode();
                        if(strStatusCode == 204 || strStatusCode == 200)
                        {
                            strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/pcpdetails/';
                            strHttpURL += +listMembersSearched.MemberId;
                            strHttpURL = strHttpURL.replaceAll('(\\s+)', '');
                            
                            if(!String.isBlank(strHttpURL))
                            {
                                /******setting header inputs in the URL*******/
                                req = new HttpRequest();
                                req.setHeader('Content-Type', 'application/json');
                                req.setHeader('Authorization', 'Bearer '+strNewResponse.access_token);
                                req.setHeader('client_id', '9b95c64f71e34399b874d63af2ff031f');
                                req.setHeader('client_secret', '4a5a56f64b474adcA86CBB58D0615406');
                                req.setEndpoint(strHttpURL);
                                req.setMethod('GET');
                                res = http.send(req);
                                
                                string strProviderResponse = res.getBody();
                
                                /******checking for response*******/
                                if(!String.isBlank(strResBody) && strResBody != null && (!strResBody.contains('NullPayload') || !strResBody.contains('Resource not found') || !strResBody.contains('Gateway Time-out')))
                                {
                                    system.debug('Body After Removal-->'+strProviderResponse);
                                
                                    
                                    System.debug('---------------+strProviderResponse:'+res.getBody());
                                    //ProviderResp = (List<ProviderJSON>)JSON.deSerialize(strProviderResponse,List<ProviderJSON>.class);
                                    
                                    for(ProviderJSON objProvider : (List<ProviderJSON>)JSON.deSerialize(strProviderResponse,List<ProviderJSON>.class)) 
                                    {  
                                        objProvider.ProviderNumber = objProvider.ProviderNumber.replaceAll('(\\s+)', '');
                                        ProviderResp.add(objProvider);
                                    }
                                    UpdatedProviderResp.add(ProviderResp[0]);
                                    
                                    if(UpdatedProviderResp[0].ProviderNumber == strProviderNum){
                                        system.debug('Different PCP-------------->'); 
                                        boolShowPCPListResp = true;
                                        boolShowPCPList = false;
                                        boolShowUpdate = TRUE;
                                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Provider Updated Successfully!');//'Provider Updated Successfully!'
                                        ApexPages.addMessage(myMsg);
                                        
                                        long strNewDate = DateTime.now().getTime() - strDate;
                                        system.debug('End Time on PCP------>'+DateTime.now().getTime());
                                        
                                        system.debug('TimeStamp on PCP-------------->'+strNewDate); 
                                        return null;    
                                    }
                                    else{
                                        system.debug('Same PCP-------------->'); 
                                        boolShowPCPListResp = false;
                                        boolShowPCPList = false;
                                        boolShowUpdate = TRUE;
                                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Not able to update as Provider got update in last 48 hours for Member# '+listMembersSearched.MemberID+'. Please try after some time!');
                                        ApexPages.addMessage(myMsg);
                                        
                                        long strNewDate = DateTime.now().getTime() - strDate;
                                        system.debug('End Time-->'+DateTime.now().getTime());
                                        
                                        system.debug('TimeStamp-------------->'+strNewDate); 
                                        return null;  
                                    }
                                    
                                }
                                else {
                                    boolShowUpdate = TRUE;
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, res.getStatus() == null ? (res.getBody() != null ? res.getBody() : 'Error Occurred') : res.getStatus());
                                    ApexPages.addMessage(myMsg);
                                    strUpdateProvider = null;
                                }
                                return null;
                            }
                        }
                    }
                }
                /******callout failed*******/
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,res.getStatus());
                ApexPages.addMessage(myMsg);
                return null;
            }
            catch(System.CalloutException e) 
            {
                system.debug('-------------catch-----------'+res.getstatus());
                boolShowResultNullMessage = TRUE;
                boolShowUpdate = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Error Code:'+res.getStatusCode()+' '+res.getstatus());//There was exception # '+listMembersSearched.MemberID);
                ApexPages.addMessage(myMsg);   
                long strNewDate = DateTime.now().getTime() - strDate;
                system.debug('End Time on PCP----->'+DateTime.now().getTime());
                system.debug('TimeStamp on PCP-------------->'+strNewDate); 
                system.debug('Oops! An Error Occurred!');
                System.debug('Callout error: '+ e);
                System.debug(res.toString());
                return null;
            }
         }
         else
         {
             system.debug('Oops! An Error Occurred! Callout failed');
             return null;
         }
    }
    
    /******************************
    ### clear data on page
    */
    public void clearPCP(){
        boolShowUpdate = FALSE;
        strActionCode = null;
        strCompanyNumber = null;
        strPCPSeen = null;
        strReasonCode = null;
        strProviderNum = null;
        //ProviderValuesUpdated = new List<Provider__c>() ;
    }
    
    /******************************
    ### traversing to previous page, maintaining member reference
    */
    public PageReference backButtonClicked(){
        PageReference objRefToMemDtlPage;
        
        objRefToMemDtlPage = Page.Member_Detail_Page_Cloned;
        objRefToMemDtlPage.getParameters().put('id',strMemberId);
        objRefToMemDtlPage.getParameters().put('strSearchedMemsJSON',strSearchedMemsJSON);
        objRefToMemDtlPage.setRedirect(true);
        return objRefToMemDtlPage;
    }
    
    /******************************
    ### traversing to member search page, maintaining member reference
    */
    public PageReference homeButtonClicked(){
        PageReference objRefToMemDtlPage;
        
        objRefToMemDtlPage = Page.HF_MemberSearch_Cloned;
        //objRefToMemDtlPage.getParameters().put('id',strMemberId);
        //objRefToMemDtlPage.getParameters().put('strSearchedMemsJSON',strSearchedMemsJSON);
        objRefToMemDtlPage.setRedirect(true);
        return objRefToMemDtlPage;
    }
    
    /******************************
    ### select provider from the display list
    */
    public void radioBtnClicked(){
        system.debug('### strMemberSelected: '+strMemberId);
        system.debug('### Provider: '+ApexPages.currentPage().getParameters().get('strMemberSelected'));
        strUpdateProvider = ApexPages.currentPage().getParameters().get('strMemberSelected');
    }    
    
    /******************************
    ### wrapper class for handling first callout response
    */
    public class Response{
        public String access_token{get;set;}   
        public String scope{get;set;}
        public String token_type{get;set;}
        public String expires_in{get;set;}
    }
    
    /******************************
    ### wrapper class for Member
    */
    public class MemberJSON{
        public String PlanEffectiveDate{get;set;}
        public String HOMPH2{get;set;}
        public String MemberID{get;set;}
        public String WRKEXT{get;set;}
        public String MemberExpirationDate{get;set;}
        public String SubscriberID{get;set;}
        public String WRKPH1{get;set;}
        public String GroupName{get;set;}
        public String BirthDate{get;set;}
        public String CellPhone{get;set;}
        public String LOB{get;set;}
        public String CompanyNumber{get;set;}
        public String HOMPH1{get;set;}
        public String EmailAddress{get;set;}
        public String City{get;set;}
        public String GroupNumber{get;set;}
        public String Gender{get;set;}
        public String WRKPH2{get;set;}
        public String AddressLine2{get;set;}
        public String ZipCode{get;set;}
        public String OtherPhone{get;set;}
        public String HOMPH3{get;set;}
        public String AddressLine1 {get;set;}
        public String BenefitPackage{get;set;}
        public String WRKPH3{get;set;}
        public String State{get;set;}
        public String PlanExpirationDate{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        
    }
    
    /******************************
    ### wrapper class for Provider Response
    */
    public class ProviderJSON{
        public String MemberID{get;set;}
        public String ProviderLastName{get;set;}
        public String ProviderFirstName{get;set;}
        public String ProviderNumber{get;set;}
        public String ExpiryDate{get;set;}
        public String ExpirationDate{get;set;}
        
    }
}