/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  09/09/2014
 * Description: This Class does below processing
 *              1.  Utility methods.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Raymundo Rodriguez        09/09/2014              Initial version.
 **************************************************************************************/
global class Utilities {
    
    //Global variables
    global static Id houseId {
        get {
            if(houseId == null) {
                for(User house : [SELECT Id FROM User WHERE Username = 'house@healthsolutions.com']) {
                    houseId = house.Id;
                }
            }
            return houseId;
        }
    }
    global static Id defaultProfilePictureId {
        get {
            if(defaultProfilePictureId == null) {
                for(Document defaultDocument : [SELECT Id FROM Document WHERE DeveloperName = 'Default_Profile_Pic']) {
                    defaultProfilePictureId = defaultDocument.Id;
                }
            }
            return defaultProfilePictureId;
        }
    }
    global static String currentDomain {
        get {
            return URL.getSalesforceBaseUrl().getHost();
        }
    }
    
    /*
    * Method name  : relateGroupsCaseObject
    * Description  : Relates in a future method Case object and Queues.
    * Return Type  : Array of String
    * Parameter    : Void.
    */
    @future
    global static void relateGroupsCaseObject(String[] groups) {
        List<QueueSObject> newQueues = new List<QueueSObject>();
        for(String groupId : groups) {
            QueueSObject queueObject = new QueueSObject();
            queueObject.SobjectType = 'Case';
            queueObject.QueueId = groupId;
            newQueues.add(queueObject);
        }
        insert newQueues;
    }
    
    /*
    * Method name  : getImageExternalURL
    * Description  : Returns External URL for Image.
    * Return Type  : String
    * Parameter    : Image Id.
    */   
    global static String getImageExternalURL(String imageId) {
        String result;
        if(String.IsNotBlank(imageId)) {
            result = 'https://' + Utilities.currentDomain.replace('visual', 'content') + '/servlet/servlet.ImageServer?id=' + 
                    imageId + '&oid=' + UserInfo.getOrganizationId();
        }
        return result;
    }
    
    /*
    * Method name  : getObjectFields
    * Description  : Returns field from an Object.
    * Return Type  : Map<String, String>
    * Parameter    : Object Name.
    */   
    global static Map<String, String> getObjectFields(String objectName) {
        Map<String, String> fields = new Map<String, String>();
        if(String.IsNotBlank(objectName)) {
            Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
            if(!objGlobalMap.containsKey(objectName)) {
                return fields;
            }
            Schema.SObjectType objectType = objGlobalMap.get(objectName);
            Map<String, Schema.SObjectField> fieldsMap = objectType.getDescribe().fields.getMap();
            for(String key : fieldsMap.keySet()) {
                Schema.DescribeFieldResult describeField = fieldsMap.get(key).getDescribe();
                fields.put(describeField.getName(), describeField.getLabel());
            }
        }
        return fields;
    }
    
    /*
    * Method name  : getPicklistValues
    * Description  : Returns values from a picklist.
    * Return Type  : List<String>
    * Parameter    : Object Name, Field Name.
    */   
    global static List<String> getPicklistValues(String objectName, String fieldName) {
        List<String> options = new List<String>();
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        if(!objGlobalMap.containsKey(objectName)) {
            return options;
        }
        Schema.SObjectType pType = objGlobalMap.get(objectName);
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(fieldName).getDescribe().getPicklistValues();
        for(Schema.PicklistEntry f : ctrl_ple) {
            options.add(f.getLabel());
        }       
        return options;
    }
    
    /*
    * Method name  :    getCountryStateValue
    * Description  :    Returns the long version of the Country/State provided.
    * Return Type  :    String
    * Parameter    :    Value.
    */
    global static String getCountryStateValue(String value) {
        String result = value;
        if(String.IsNotBlank(value)) {
            CountryStateMapping__c mappingResult = CountryStateMapping__c.getAll().get(value);
            if(mappingResult != null) {
                result = mappingResult.Complete_Name__c;
            }
        }
        return result;
    }
    
    /*
    * Method name  :    getLogosAndPanels
    * Description  :    Returns the wrapper map of Logos and Panels.
    * Return Type  :    Map<String, LogosAndPanels>
    * Parameter    :    -
    */
    global static Map<String, LogosAndPanels> getLogosAndPanels() {
        Map<String, LogosAndPanels> result = new Map<String, LogosAndPanels>();
        Set<String> apiNames = new Set<String>();
        
        //Getting Document API names.
        for(String key : Logos_Panels__c.getAll().keySet()) {
            
            //API names.
            Logos_Panels__c customSetting = Logos_Panels__c.getAll().get(key);
            apiNames.add(customSetting.Document_API_Name__c);
            
            //Setting result.
            LogosAndPanels wrapper = new LogosAndPanels();
            wrapper.customSetting = customSetting;
            result.put(customSetting.Name, wrapper);
        }
        
        //Getting Document Ids.
        for(Document document : [SELECT Id, DeveloperName FROM Document WHERE DeveloperName IN :apiNames]) {
            for(String key : result.keySet()) {
                LogosAndPanels wrapper = result.get(key);
                if(wrapper.customSetting.Document_API_Name__c == document.DeveloperName) {
                    wrapper.documentId = document.Id;
                }
            }
        }
        return result;
    }
    
    /*
    * Class name   :    LogosAndPanels
    * Description  :    Wrapper class for Logos_Panels__c custom setting.
    * Return Type  :    -
    * Parameter    :    -
    */
    global class LogosAndPanels {
        
        //Variables.
        global Logos_Panels__c customSetting {get; set;}
        global String documentId {get; set;}
    }
    
    /*
    * Method name  : getDependentOptions
    * Description  : Returns dependent values from a picklist.
    * Return Type  : Map<String,List<String>>
    * Parameter    : Object Name, Controlling Field Name, Dependent Field Name.
    */
    public static Map<String,List<String>> getDependentOptions(String objectName, String controllingFieldName, String dependentFieldName){
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        if (!objGlobalMap.containsKey(objectName)) {
            return objResults;
        }
        Schema.SObjectType pType = objGlobalMap.get(objectName);
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        if(!objFieldMap.containsKey(controllingFieldName) || !objFieldMap.containsKey(dependentFieldName)) {
            return objResults;
        }
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(controllingFieldName).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> dep_ple = objFieldMap.get(dependentFieldName).getDescribe().getPicklistValues();
        Bitset objBitSet = new Bitset();
        for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++) {          
            Schema.PicklistEntry ctrl_entry = ctrl_ple[pControllingIndex];
            String pControllingLabel = ctrl_entry.getLabel();
            objResults.put(pControllingLabel,new List<String>());
        }
        objResults.put('',new List<String>());
        objResults.put(null,new List<String>());
        for(Integer pDependentIndex=0; pDependentIndex<dep_ple.size(); pDependentIndex++) {
            Schema.PicklistEntry dep_entry = dep_ple[pDependentIndex];
            String pEntryStructure = JSON.serialize(dep_entry);                
            TPicklistEntry objDepPLE = (TPicklistEntry) JSON.deserialize(pEntryStructure, TPicklistEntry.class);
            if (objDepPLE.validFor==null || objDepPLE.validFor=='') {
                continue;
            }
            for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){    
                if (objBitSet.testBit(objDepPLE.validFor,pControllingIndex)) {
                    String pControllingLabel = ctrl_ple[pControllingIndex].getLabel();
                    objResults.get(pControllingLabel).add(objDepPLE.label);
                }
            }
        } 
        return objResults;
    }
    
    /*
    * Method name  : Bitset
    * Description  : Class which process base64 values.
    * Return Type  : -
    * Parameter    : -
    */
    public class Bitset{
        
        //Global variables.
        public Map<String,Integer> AlphaNumCharCodes {get;set;}
        public Map<String, Integer> Base64CharCodes { get; set; }
        
        /*
        * Method name  : Bitset
        * Description  : Constructor of the class.
        * Return Type  : -
        * Parameter    : -
        */
        public Bitset(){
            LoadCharCodes();
        }
        
        /*
        * Method name  : LoadCharCodes
        * Description  : Loads Char codes.
        * Return Type  : -
        * Parameter    : -
        */
        private void LoadCharCodes(){
            AlphaNumCharCodes = new Map<String,Integer>{
                'A'=>65,'B'=>66,'C'=>67,'D'=>68,'E'=>69,'F'=>70,'G'=>71,'H'=>72,'I'=>73,'J'=>74,
                'K'=>75,'L'=>76,'M'=>77,'N'=>78,'O'=>79,'P'=>80,'Q'=>81,'R'=>82,'S'=>83,'T'=>84,
                'U'=>85,'V'=> 86,'W'=>87,'X'=>88,'Y'=>89,'Z'=>90    
            };
            Base64CharCodes = new Map<String, Integer>();
            Set<String> pUpperCase = AlphaNumCharCodes.keySet();
            for(String pKey : pUpperCase){
                AlphaNumCharCodes.put(pKey.toLowerCase(),AlphaNumCharCodes.get(pKey)+32);
                Base64CharCodes.put(pKey,AlphaNumCharCodes.get(pKey) - 65);
                Base64CharCodes.put(pKey.toLowerCase(),AlphaNumCharCodes.get(pKey) - (65) + 26);
            }
            for (Integer i=0; i<=9; i++){
                AlphaNumCharCodes.put(string.valueOf(i),i+48);
                Base64CharCodes.put(string.valueOf(i), i + 52);
            }
        }
        
        /*
        * Method name  : testBit
        * Description  : Determines if the comparison is valid.
        * Return Type  : Boolean
        * Parameter    : Value, Number.
        */
        public Boolean testBit(String pValidFor,Integer n){
            List<Integer> pBytes = new List<Integer>();
            Integer bytesBeingUsed = (pValidFor.length() * 6)/8;
            Integer pFullValue = 0;
            if (bytesBeingUsed <= 1) {
                return false;
            }
            Integer bit = 7 - (Math.mod(n,8)); 
            Integer targetOctet = (bytesBeingUsed - 1) - (n >> bytesBeingUsed); 
            Integer shiftBits = (targetOctet * 8) + bit;
            for(Integer i=0;i<pValidFor.length();i++){
                pBytes.Add((Base64CharCodes.get((pValidFor.Substring(i, i+1)))));
            }
            for (Integer i = 0; i < pBytes.size(); i++) {
                Integer pShiftAmount = (pBytes.size()-(i+1))*6;
                pFullValue = pFullValue + (pBytes[i] << (pShiftAmount));
            }
            Integer tBitVal = ((Integer)(Math.Pow(2, shiftBits)) & pFullValue) >> shiftBits;
            return  tBitVal == 1;
        }
    }
    
    /*
    * Method name  : TPicklistEntry
    * Description  : Wrapper class for picklist values.
    * Return Type  : -
    * Parameter    : -
    */
    public class TPicklistEntry{
        public string active {get;set;}
        public string defaultValue {get;set;}
        public string label {get;set;}
        public string value {get;set;}
        public string validFor {get;set;}
    }
}