public Class SearchClaimsController
    {
        public String strMemberID{get;set;}
        public String strProvierNumber{get;set;}
        public String strCheckNumber{get;set;}
        public String strClaimsNumber{get;set;}
        public String strExternalClaimsNumber{get;set;}
        public Date dateDateFrom{get;set;}
        public Date dateDateTo{get;set;}
        public Boolean boolShowClaims{get;set;}

        public String strMemberName{get;set;}
        public Date dateBirthDate{get;set;}
        public String strProviderFirstName{get;set;}
        public String strProviderLastName{get;set;}
        public String strVendorNumber{get;set;}
        public Date dateReceivedDate{get;set;}
        public Date dateEnteredDate{get;set;}
        public Date datePostDate{get;set;}
        public Date strPaidDate{get;set;}
        public String strStatus{get;set;}
        public String strHttpURL;
        public Boolean boolShowCriteriaNullMessage {get;set;}
        public Boolean boolShowResultNullMessage {get;set;}
        public List<Claim__c> ClaimValues{get;set;}
        public List<Claim__c> ClaimValuesUpdated{get;set;}
        public Set<ID> setContactId;
        public Set<ID> setPhysicianId;
        public Map<ID,Contact> mapClaimVsContactId = new Map<ID,Contact>();
        public Map<ID,Contact> mapClaimVsPhysicianId = new Map<ID,Contact>();
        public Integer intCheckNumber;
       
        public SearchClaimsController(ApexPages.StandardController con)
        {
            boolShowClaims = FALSE;
        }
        
        public Void searchClaims()
        {
        
            ClaimValuesUpdated = new List<Claim__C>();
            //Check for the params passing from the page. Make sure at least one param is NOT NULL
            
            if(!String.isBlank(strMemberId) || !String.isBlank(strProvierNumber) || !String.isBlank(strClaimsNumber) || !String.isBlank(strExternalClaimsNumber)||!String.isBlank(strCheckNumber))
            {    
                if(!String.isBlank(strCheckNumber))
                {
                    intCheckNumber = Integer.valueOf(strCheckNumber);
                }
                else
                {
                    intCheckNumber = NULL;
                }
                //Create the URL to hit.
                boolShowClaims = TRUE;    
                strHttpURL = 'http://restwebservicehfcrm.cloudhub.io/mulerestservice?MemberId='+strMemberId+'&Provider='+strProvierNumber+
                '&Check='+intCheckNumber+'&Claim='+strClaimsNumber+'&DateFrom=&DateTo=&ExtClaim='+strExternalClaimsNumber;
                system.debug('The Endpoint URL is-->'+strHttpURL);
            }
            else
            {
                boolShowCriteriaNullMessage = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please Enter Atleast One Search Criteria');
                ApexPages.addMessage(myMsg);
            }         
               
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            if(!String.isBlank(strHttpURL))
            {
                req.setEndpoint(strHttpURL);
                req.setMethod('GET');
            
                try 
                {
                    system.debug('THE REQ-->'+req.getBody());
                    res = http.send(req);
                    system.debug('THE RES-->'+res.getBody());
                    String strResBody = res.getBody();
                    String strUnwanted = ',"type":"Claim__c"';
                   
                   /*
                    String strRegEx = '/(,")\btype(":")([A-Z][a-z]*)(_*c)(",)/g';
                    String strReplacement = ',';
                    //Remove the UNWANTED type parameter being passed by MuleSoft
                    if(!String.isBlank(strResBody))
                    {
                         strResBody = strResBody.replaceAll(strRegEx,strReplacement);
                         system.debug('New Body-->'+strResBody);
                    }
                   */
                   
                    if(!String.isBlank(strResBody))
                    {
                         strResBody = strResBody.remove(strUnwanted);
                         system.debug('Body After Removal-->'+strResBody);
                    }
                    
                    ClaimValues = (List<Claim__c>)JSON.deSerialize(strResBody,List<Claim__c>.class);
                    
                    for(Claim__c objClaim : ClaimValues) 
                    {
                         for(Contact objCon : [Select Id, Name, Birthdate, Member_Id__c  FROM Contact Where Id = :objClaim.Contact__c])
                         {
                            
                             objClaim.Contact__r = objCon;
                         }
                         
                         for(Contact objCon : [Select Id, FirstName, LastName FROM Contact Where Id = :objClaim.Physician__c])
                         {
                             objClaim.Physician__r = objCon;
                         }
                         
                         ClaimValuesUpdated.add(objClaim);
                    }
                    
                    system.debug(ClaimValues);
                }
                 
                catch(System.CalloutException e) 
                {
                    boolShowResultNullMessage = TRUE;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Oops! An Error Occurred.');
                    ApexPages.addMessage(myMsg);   
                    system.debug('Oops! An Error Occurred!');
                    System.debug('Callout error: '+ e);
                    System.debug(res.toString());
                }
             }
             else
             {
                 system.debug('Oops! An Error Occurred!');
             }
            
       }
    }