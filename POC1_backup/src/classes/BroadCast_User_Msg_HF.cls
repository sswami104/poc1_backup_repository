/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  01-Oct-2015
 * Description: This Class does below processing
 *              1. Select Category
 *              2. Assign message based on selection
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            01-Oct-2015              Initial version.
 **************************************************************************************/
public Class BroadCast_User_Msg_HF
{	
	public List<HF_User_Messages_Association__c> lstUserMsgAssg{get;set;}
	public List<HF_User_Messages_Association__c> lstBookmark{get;set;}
	public HF_User_Messages_Association__c objMessage{get;set;}
	public HF_User_Messages_Association__c objAckMsg{get;set;}
	public HF_User_Messages_Association__c objBookMsg{get;set;}
	public HF_User_Messages_Association__c objSnoozeCheck{get;set;}
	public User objUser{get;set;}
	public string strAck{get;set;}
	public string strSnooze{get;set;}
	public string strBook{get;set;}
	public string strMessage{get;set;}
	public Boolean strBookmark{get;set;}
	public Boolean strMessages{get;set;}
	public Boolean strBoolSnooze{get;set;}
	public Boolean strSnoozeDisabled{get;set;}
	
	public BroadCast_User_Msg_HF(){
	    objUser = [select id, profile.name from USER where id =:UserInfo.getUserId()];
		if(objUser.profile.name=='System Administrator'){
		   defaultSnoozeList(); 
		}
		else{
		   defaultList();
		}
	}
	
	public void defaultSnoozeList(){
	    strBookmark = false;
	    strBoolSnooze = true;
	    strMessages = true;
	    system.debug('test');
	    lstUserMsgAssg = new List<HF_User_Messages_Association__c>();
		lstUserMsgAssg = [select id, HF_Broadcast_Message__c,Snooze_HF__c, Snooze_Disable_HF__c, HF_Broadcast_Message__r.HF_Broadcast_Message_Name__c, HF_Broadcast_Message__r.HF_Start_Date__c, HF_Broadcast_Message__r.HF_End_Date__c from HF_User_Messages_Association__c where HF_User__c=:UserInfo.getUserId() AND HF_Broadcast_Message__r.HF_End_Date__c >= :system.today() AND HF_Acknowledgement__c =false AND HF_Broadcast_Message__r.Recordtype.DeveloperName=:System.Label.Broadcast_Note_HF];
		system.debug('lstUserMsgAssg-------->'+lstUserMsgAssg);
	}
	
	public void defaultList(){
	    strBookmark = false;
	    strMessages = true;
	    system.debug('test');
	    lstUserMsgAssg = new List<HF_User_Messages_Association__c>();
		lstUserMsgAssg = [select id, HF_Broadcast_Message__c,Snooze_HF__c, Snooze_Disable_HF__c, HF_Broadcast_Message__r.HF_Broadcast_Message_Name__c, HF_Broadcast_Message__r.HF_Start_Date__c, HF_Broadcast_Message__r.HF_End_Date__c from HF_User_Messages_Association__c where HF_User__c=:UserInfo.getUserId() AND HF_Broadcast_Message__r.HF_End_Date__c >= :system.today() AND HF_Acknowledgement__c =false AND HF_Broadcast_Message__r.Recordtype.DeveloperName=:System.Label.Broadcast_Note_HF];
		system.debug('lstUserMsgAssg-------->'+lstUserMsgAssg);
	}
	
	public void snooze(){
	    objSnoozeCheck = new HF_User_Messages_Association__c();
	    objSnoozeCheck = [select id, Snooze_HF__c from HF_User_Messages_Association__c where id =:strSnooze];
	    if(objSnoozeCheck != null){
	        System.debug('### ---'+objSnoozeCheck.Snooze_HF__c);
    	    objSnoozeCheck.Snooze_HF__c +=1;
    	    update objSnoozeCheck;
    	    lstUserMsgAssg = new List<HF_User_Messages_Association__c>();
    	    lstUserMsgAssg = [select id, HF_Broadcast_Message__c,Snooze_HF__c, Snooze_Disable_HF__c, HF_Broadcast_Message__r.HF_Broadcast_Message_Name__c, HF_Broadcast_Message__r.HF_Start_Date__c, HF_Broadcast_Message__r.HF_End_Date__c from HF_User_Messages_Association__c where HF_User__c=:UserInfo.getUserId() AND HF_Broadcast_Message__r.HF_End_Date__c >= :system.today() AND HF_Acknowledgement__c =false AND id !=:objSnoozeCheck.id AND HF_Broadcast_Message__r.Recordtype.DeveloperName=:System.Label.Broadcast_Note_HF];
	    }
	}
	
	public void bookmarkList(){
	    strMessages = false;
	    strBookmark = true;
	    system.debug('test');
		lstBookmark = [select id, HF_Broadcast_Message__c, HF_Broadcast_Message__r.HF_Broadcast_Message_Name__c, HF_Broadcast_Message__r.HF_Start_Date__c, HF_Broadcast_Message__r.HF_End_Date__c from HF_User_Messages_Association__c where HF_User__c=:UserInfo.getUserId() AND Bookmark_HF__c =true AND HF_Broadcast_Message__r.Recordtype.DeveloperName=:System.Label.Broadcast_Note_HF];//HF_Broadcast_Message__r.HF_End_Date__c >= :system.today() AND 
		system.debug('lstBookmark-------->'+lstBookmark);
	}
	
	public void gotoMessage(){
	    /*objMessage = new HF_User_Messages_Association__c();
	    system.debug('strMessage------------>'+strMessage);
	    objMessage = [select id, HF_Broadcast_Message__r.HF_Broadcast_Message_Name__c from HF_User_Messages_Association__c where id =:strMessage];
	    system.debug('objMessage------------>'+objMessage);
	    window.location.href = '/'+objMessage.HF_Broadcast_Message__r.HF_Broadcast_Message_Name__c;*/
	}
	
	public void Acknowledged(){
	    system.debug('strAck-------->'+strAck);
	    objAckMsg = new HF_User_Messages_Association__c(id=strAck);
	    objAckMsg.HF_Acknowledgement__c = true;
	    objAckMsg.Acknowledge_Timestamp_HF__c = system.now();
	    update objAckMsg;
	    defaultList();
	    
	}
	
	public void Bookmarked(){
	    system.debug('strBook-------->'+strBook);
	    objBookMsg = new HF_User_Messages_Association__c(id=strBook);
	    objBookMsg.Bookmark_HF__c = true;
	    update objBookMsg;
	    defaultList();
	}
}