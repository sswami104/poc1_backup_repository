/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  10-Aug-2015
 * Description: This Class does below processing
 *              1. Search for member.
 *              2. Change PCP redirect
 *              3. Claims and Authorization fetxh.
 *  
 *  Modification Log :
 *  -----------------------------------------------------------------------------
 *  * Developer                 Date                    Description
 *  * ----------------------------------------------------------------------------                 
 *  * Rahul Saxena            10-Aug-2015              Initial version.
 **************************************************************************************/
public with sharing class MemberDetail{
    //Global Variables.

    public String mmbrId;
    public MemberJSON con{get;set;}    
    
    public String strMemberId{get;set;}
    public String strProvierNumber{get;set;}
    public String strCheckNumber{get;set;}
    public String strClaimsNumber{get;set;}
    public String strExternalClaimsNumber{get;set;}
    public Boolean boolShowClaims{get;set;}
    public Boolean boolAuthInfo{get;set;}
    public Boolean boolClaimsInfo{get;set;}
    
    public String strHttpURL;
    public Boolean boolShowCriteriaNullMessage {get;set;}
    public Boolean boolShowResultNullMessage {get;set;}
    public List<ClaimsJSON> ClaimValues{get;set;}
    public List<ClaimsJSON> ClaimValuesUpdated{get;set;}
    public List<AuthJSON> AuthValues{get;set;}
    public List<AuthJSON> AuthValuesUpdated{get;set;} 
    public MemberJSON listMembersSearched{get; set;}
    public Contact dummyContact1 {get; set;}
    public Contact dummyContact2 {get; set;}
    public Contact dummyContact3 {get; set;}
    public Contact dummyContact4 {get; set;}
    public String strSearchedMemsJSON;
    
    public final Integer PAGE_SIZE = 2;
    //List<Claim__c> listClaims = new List<Claim__c>();
   /* public ApexPages.StandardSetController setConSearchResults{
        get{
            if(listClaims.size() == 0){
                listClaims = (ClaimValues == null)? new List<Claim__c>() : ClaimValues;
                setConSearchResults = new ApexPages.StandardSetController(listClaims);
                setConSearchResults.setPageSize(PAGE_SIZE);
                setConSearchResults.setPageNumber(1);
            }
            return setConSearchResults;
        }
        set;
    }
    public List<Claim__c> ClaimValuesUpdated{
        get{
            return (List<Claim__c>)setConSearchResults.getRecords();
        }
        set;
    }*/
    public MemberDetail()
    {   
        mmbrId=ApexPages.CurrentPage().getParameters().get('id');
        strMemberID = mmbrId;
        dummyContact1 = new Contact();
        dummyContact2 = new Contact();
        dummyContact3 = new Contact();
        dummyContact4 = new Contact();
        //mmbrId = '00355000004nkRg';//### hardcoding for implementing pagination - need to remove later
        if(ApexPages.CurrentPage().getParameters().containsKey('strSearchedMemsJSON') && String.isNotBlank(ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON'))){
            strSearchedMemsJSON = ApexPages.CurrentPage().getParameters().get('strSearchedMemsJSON');
            list<MemberJSON> con = new List<MemberJSON>();
            con = (List<MemberJSON>)JSON.deSerialize(strSearchedMemsJSON,List<MemberJSON>.class);
            system.debug('### listMembersSearched.size: '+listMembersSearched);
            listMembersSearched = new MemberJSON();
            listMembersSearched = con[0];
        }
        /*if(mmbrId<> NULL){
           con = [select Id, Member_Id__c, firstname, lastname, Birthdate from Contact where id= :mmbrId];
        } */
        
        boolShowClaims = TRUE;  
        boolClaimsInfo = FALSE;
        boolAuthInfo = FALSE;     
    }
        
    public PageReference ChangePCP() {
        PageReference objRefToChangePCP;
        
        objRefToChangePCP = Page.HF_ChangePCP;
        objRefToChangePCP.getParameters().put('id',mmbrId);
        objRefToChangePCP.getParameters().put('retURL','/apex/Member_Detail_Page?id='+mmbrId);
        objRefToChangePCP.getParameters().put('strSearchedMemsJSON',strSearchedMemsJSON);
        objRefToChangePCP.setRedirect(true);
        return objRefToChangePCP;
    }
    //for Calims response
    public void CustomFunction() {
        ClaimValuesUpdated = new List<ClaimsJSON>();
            //Check for the params passing from the page. Make sure at least one param is NOT NULL
            
            if(!String.isBlank(listMembersSearched.memberId)||dummyContact1.BirthDate != null||dummyContact2.BirthDate != null)
            {                    
                boolShowClaims = TRUE;    
               strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/claims?';
           //     strHttpURL = 'http://mocksvc.mulesoft.com/mocks/f4cb99ac-0b46-4675-a7c0-507c96fcfb76/api/auths?endDate=';   //{ endDate }&memberId={ memberId }&startDate={startDate}
                //end date or TO date
            // strHttpURL += (dummyContact2.BirthDate != null) ? '&enddate='+(String.valueOf(dummyContact2.BirthDate.Year())).substring(0,1)+(dummyContact2.BirthDate.month()>9?'-':'-0')+dummyContact2.BirthDate.month()+(dummyContact2.BirthDate.day()>9?'-':'-0')+dummyContact2.BirthDate.day() : '';
              //  strHttpURL += (dummyContact2.BirthDate != null) ? '&enddate='+String.valueOf(dummyContact2.BirthDate.Year()).substring(0,1)+String.valueOf(dummyContact2.BirthDate.Year()).substring(2,3)+dummyContact2.BirthDate.month()+dummyContact2.BirthDate.day();
               strHttpURL += (dummyContact2.BirthDate != null) ? 'endDate='+(String.valueOf(dummyContact2.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact2.BirthDate.Year())).substring(2,4)+(dummyContact2.BirthDate.month()>9?'':'0')+dummyContact2.BirthDate.month()+(dummyContact2.BirthDate.day()>9?'':'0')+dummyContact2.BirthDate.day() : '';
              //  strHttpURL +='&memberId=';
                strHttpURL += (listMembersSearched.MemberId != null && listMembersSearched.MemberId != '') ? '&memberId='+listMembersSearched.MemberId : '';
               strHttpURL += (dummyContact1.BirthDate != null) ? '&startDate='+(String.valueOf(dummyContact1.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact1.BirthDate.Year())).substring(2,4)+(dummyContact1.BirthDate.month()>9?'':'0')+dummyContact1.BirthDate.month()+(dummyContact1.BirthDate.day()>9?'':'0')+dummyContact1.BirthDate.day() : '';
              
               // strHttpURL += '&startDate=';
                //start date or From date
               // strHttpURL += (dummyContact1.BirthDate != null) ? '&startDate='+(String.valueOf(dummyContact1.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact1.BirthDate.Year())).substring(2,3)+dummyContact1.BirthDate.month()+dummyContact1.BirthDate.day();
                
                //strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/claims?endDate=2022-07-22T00:00:00.000Z&memberId=9837&startDate=1975-07-22T00:00:00.000Z';
                system.debug('The Endpoint URL is-->'+strHttpURL);
            }
            else
            {
                boolShowCriteriaNullMessage = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please Enter Atleast One Search Criteria');
                ApexPages.addMessage(myMsg);
            }         
               
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            if(!String.isBlank(strHttpURL))
            {
                req.setEndpoint(strHttpURL);
                req.setMethod('GET');
                req.setTimeOut(120000);
            
                try 
                {
                    system.debug('THE REQ-->'+req.getBody());
                    res = http.send(req);
                    system.debug('THE RES-->'+res.getBody());
                    String strResBody = res.getBody();
                    String strUnwanted = ',"type":"Claim__c"';                                      
                   
                    if(!String.isBlank(strResBody))
                    {
                         strResBody = strResBody.remove(strUnwanted);
                         system.debug('Body After Removal-->'+strResBody);
                    }
                    
                    ClaimValues = (List<ClaimsJSON>)JSON.deSerialize(strResBody,List<ClaimsJSON>.class);
                    
                    for(ClaimsJSON objClaim : ClaimValues) 
                    {                         
                         ClaimValuesUpdated.add(objClaim);
                    }
                    
                    system.debug('ClaimValuesUpdated-------------->'+ClaimValuesUpdated);
                    boolClaimsInfo = TRUE;
                }
                 
                catch(System.CalloutException e) 
                {
                    boolShowResultNullMessage = TRUE;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Oops! An Error Occurred.');
                    ApexPages.addMessage(myMsg);   
                    system.debug('Oops! An Error Occurred!');
                    System.debug('Callout error: '+ e);
                    System.debug(res.toString());
                }
             }
             else
             {
                 system.debug('Oops! An Error Occurred!');
             }
            
       }
       public PageReference backButtonClicked(){
            PageReference objRefToMemSearch;
            
            objRefToMemSearch = Page.HF_MemberSearch;
            objRefToMemSearch.getParameters().put('id',strMemberID);
            objRefToMemSearch.setRedirect(true);
            return objRefToMemSearch;
       }
       
       //method for clearing claims
       public void ClearClaims(){
           ClaimValuesUpdated = new List<ClaimsJSON>();
           dummyContact1.BirthDate = null;
           dummyContact2.BirthDate = null;
       }
       
       //for Auth response
       public void CustomFunction1() {
        AuthValuesUpdated = new List<AuthJSON>();
            //Check for the params passing from the page. Make sure at least one param is NOT NULL
            
            if(!String.isBlank(listMembersSearched.MemberId )||dummyContact3.BirthDate != null||dummyContact4.BirthDate != null)
            {                    
                boolShowClaims = TRUE;    
                //strHttpURL = 'http://genericservice.cloudhub.io/genericService?reqdata=auth&MemberId='+con.Member_Id__c+'&firstname='+con.firstname+'&lastname='+con.lastname+'&dob='+con.Birthdate.Year()+(con.Birthdate.month()>9?'-':'-0')+con.Birthdate.month()+(con.Birthdate.day()>9?'-':'-0')+con.Birthdate.day();
                //strHttpURL = 'http://genericservice.cloudhub.io/genericService?reqdata=auth';
                /*
                strHttpURL = 'http://mocksvc.mulesoft.com/mocks/f4cb99ac-0b46-4675-a7c0-507c96fcfb76/api/claims?endDate=';          //{ endDate }&memberId={ memberId }&startDate={ startDate }';
                //end date or TO date
                strHttpURL += (dummyContact4.BirthDate != null) ? '&enddate='+dummyContact4.BirthDate.Year()+(dummyContact4.BirthDate.month()>9?'-':'-0')+dummyContact4.BirthDate.month()+(dummyContact4.BirthDate.day()>9?'-':'-0')+dummyContact4.BirthDate.day() : '';
                strHttpURL +='&memberId=';
                strHttpURL += (con.strMemberId != null && con.strMemberId != '') ? '&memberid='+con.strMemberId : '';
                strHttpURL += '&startDate=';
                //start date or From date
                strHttpURL += (dummyContact3.BirthDate != null) ? '&startdate='+dummyContact3.BirthDate.Year()+(dummyContact3.BirthDate.month()>9?'-':'-0')+dummyContact3.BirthDate.month()+(dummyContact3.BirthDate.day()>9?'-':'-0')+dummyContact3.BirthDate.day() : '';
                */
                
                //for mock data
                //strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/auths?endDate=2022-07-22T00:00:00.000Z&memberId=9837&startDate=1975-07-22T00:00:00.000Z';
                strHttpURL = 'http://hf-eai-sfdc-poc-proxy.cloudhub.io/auths?';
                strHttpURL += (dummyContact4.BirthDate != null) ? 'endDate='+(String.valueOf(dummyContact4.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact4.BirthDate.Year())).substring(2,4)+(dummyContact4.BirthDate.month()>9?'':'0')+dummyContact4.BirthDate.month()+(dummyContact4.BirthDate.day()>9?'':'0')+dummyContact4.BirthDate.day() : '';
                //strHttpURL += (dummyContact4.BirthDate != null) ? 'endDate='+dummyContact4.BirthDate : '';
                strHttpURL += (listMembersSearched.MemberId  != null && listMembersSearched.MemberId  != '') ? '&memberId='+listMembersSearched.MemberId : '';
                strHttpURL += (dummyContact3.BirthDate != null) ? '&startDate='+(String.valueOf(dummyContact3.BirthDate.Year())).substring(0,1)+(String.valueOf(dummyContact3.BirthDate.Year())).substring(2,4)+(dummyContact3.BirthDate.month()>9?'':'0')+dummyContact3.BirthDate.month()+(dummyContact3.BirthDate.day()>9?'':'0')+dummyContact3.BirthDate.day() : '';
                //strHttpURL += (dummyContact3.BirthDate != null) ? '&startdate='+dummyContact3.BirthDate : '';
                
                
                //2022-07-22T00:00:00.000Z&memberId=9837&startDate=1975-07-22T00:00:00.000Z';
                //strHttpURL += (con.Member_Id__c != null && con.Member_Id__c != '') ? '&memberid='+con.Member_Id__c : '';
                //strHttpURL += (con.firstname != null && con.firstname != '') ? '&firstname='+con.firstname : '';
                //strHttpURL += (con.lastname != null && con.lastname != '') ? '&lastname='+con.lastname : '';
                //strHttpURL += (con.Birthdate != null) ? '&dob='+con.Birthdate.Year()+(con.Birthdate.month()>9?'-':'-0')+con.Birthdate.month()+(con.Birthdate.day()>9?'-':'-0')+con.Birthdate.day() : '';
                system.debug('The Endpoint URL is-->'+strHttpURL);
            }
            else
            {
                boolShowCriteriaNullMessage = TRUE;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please Enter Atleast One Search Criteria');
                ApexPages.addMessage(myMsg);
            }         
               
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            if(!String.isBlank(strHttpURL))
            {
                req.setEndpoint(strHttpURL);
                req.setMethod('GET');
                req.setTimeOut(120000);
            
                try 
                {
                    system.debug('THE REQ-->'+req.getBody());
                    res = http.send(req);
                    system.debug('THE RES-->'+res.getBody());
                    String strResBody = res.getBody();
                    String strUnwanted = ',"type":"Authorization__c"';                                      
                   
                    if(!String.isBlank(strResBody))
                    {
                         strResBody = strResBody.remove(strUnwanted);
                         system.debug('Body After Removal-->'+strResBody);
                    }
                    
                    AuthValues = (List<AuthJSON>)JSON.deSerialize(strResBody,List<AuthJSON>.class);
                    
                    for(AuthJSON objAuth : AuthValues) 
                    {                         
                         AuthValuesUpdated.add(objAuth);
                    }
                    
                    system.debug(AuthValues);
                    boolAuthInfo = TRUE;
                }
                 
                catch(System.CalloutException e) 
                {
                    boolShowResultNullMessage = TRUE;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Oops! An Error Occurred.');
                    ApexPages.addMessage(myMsg);   
                    system.debug('Oops! An Error Occurred!');
                    System.debug('Callout error: '+ e);
                    System.debug(res.toString());
                }
             }
             else
             {
                 system.debug('Oops! An Error Occurred!');
             }
       }
       
       public void ClearAuth(){
           AuthValuesUpdated = new List<AuthJSON>();
           dummyContact3.BirthDate = null;
           dummyContact4.BirthDate = null;
       }
       
      public class MemberJSON{
        public String PlanEffectiveDate{get;set;}
        public String HOMPH2{get;set;}
        public String MemberID{get;set;}
        public String WRKEXT{get;set;}
        public String MemberExpirationDate{get;set;}
        public String SubscriberID{get;set;}
        public String WRKPH1{get;set;}
        public String GroupName{get;set;}
        public String BirthDate{get;set;}
        public String CellPhone{get;set;}
        public String LOB{get;set;}
        public String CompanyNumber{get;set;}
        public String HOMPH1{get;set;}
        public String EmailAddress{get;set;}
        public String City{get;set;}
        public String GroupNumber{get;set;}
        public String Gender{get;set;}
        public String WRKPH2{get;set;}
        public String AddressLine2{get;set;}
        public String ZipCode{get;set;}
        public String OtherPhone{get;set;}
        public String HOMPH3{get;set;}
        public String AddressLine1 {get;set;}
        public String BenefitPackage{get;set;}
        public String WRKPH3{get;set;}
        public String State{get;set;}
        public String PlanExpirationDate{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        
    }
    //Claims wrapper class
    public class ClaimsJSON{
        /*
        public String toProviderLastName{get;set;}   
        public String toProviderId{get;set;}
        public String toProviderFirstName{get;set;}
        public String fromProviderLastName{get;set;}
        public String fromProviderId{get;set;}   
        public String fromProviderFirstName{get;set;}
        public String expDate{get;set;}
        public String effDate{get;set;}
        public String authNo{get;set;}      */
        public String PayAmount {get;set;}
        public String ClaimAmount {get;set;}
        public String AllowedAmount{get;set;}
        public String CopayAmount {get;set;}
        public String PrepayAmount{get;set;}
        public String DeductibleAmount{get;set;}
        public String COBAmount{get;set;}
        public String CoinsuranceAmount{get;set;}
        public String WithholdAmount{get;set;}

    }
    //Auth wrapper class
    public class AuthJSON{
        
        /*public String toProviderLastName{get;set;}   
        public String toProviderId{get;set;}
        public String toProviderFirstName{get;set;}
        public String fromProviderLastName{get;set;}
        public String fromProviderId{get;set;}   
        public String fromProviderFirstName{get;set;}*/
        public String expDate{get;set;}
        public String effDate{get;set;}
        public String authNo{get;set;}   
        
        public String AuthorizationNumber{get;set;} 
        public String EffectiveDate{get;set;} 
        public String ExpirationDate{get;set;}  
        
        public String FromProviderID{get;set;} 
        public String FromProviderLastName{get;set;} 
        public String FromProviderFirstName{get;set;} 
        
        
        public String ToProviderID{get;set;} 
        public String ToProviderLastName{get;set;} 
        public String ToProviderFirstName {get;set;} 
        
            }
}