public with sharing class testMemSearchCont{

    public testMemSearchCont()
    {
    
        system.debug('before');
        for(Contact objCon : [select id, Name, FirstName, LastName from Contact limit 5])
        system.debug('objCon-------->'+objCon.FirstName+'     '+objCon.LastName);
        
        UtilityClass.test1();
        
        system.debug('after');
        for(Contact objCon : [select id, Name, FirstName, LastName from Contact limit 5])
        system.debug('objCon-------->'+objCon.FirstName+'     '+objCon.LastName);
        
        system.debug('in the end');
        //String searchquery= FIND {FirstName} IN ALL FIELDS RETURNING Contact(Id, FirstName) limit 3;
        Search.SearchResults searchResults = Search.find('FIND \'FirstName LastName\' IN ALL FIELDS RETURNING Contact(Id, FirstName,LastName) limit 3');
        system.debug('searchResults -------->'+searchResults);
        List<Search.SearchResult> articlelist = searchResults.get('Contact');
        for(Search.SearchResult searchResult : articlelist ){
            Contact objCon = (Contact)searchResult.getSObject();
            system.debug('objCon-------->'+objCon.FirstName+'     '+objCon.LastName);
        }
    }

}