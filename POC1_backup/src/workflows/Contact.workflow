<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_for_Encrypted_Field_Test</fullName>
        <description>Email Alert for Encrypted Field Test</description>
        <protected>false</protected>
        <recipients>
            <recipient>kedesai@hfcrm</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Encrypted_Field_Test</template>
    </alerts>
    <rules>
        <fullName>Encrypted Field Test</fullName>
        <actions>
            <name>Email_Alert_for_Encrypted_Field_Test</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Member_Id__c</field>
            <operation>equals</operation>
            <value>112358</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.FirstName</field>
            <operation>equals</operation>
            <value>Kevin</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
